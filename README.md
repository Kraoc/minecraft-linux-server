# Minecraft Linux Server

For primary use on a Raspebby PI, these Linux shell scripts permit to manage a Minecraft server.

Just place them in `/opt/minecraft` folder, run `bash minecraft` to initiate base things and type `bash minecraft help` to learn.


## Inside

Use [Paper](https://papermc.io) optimized Minecraft server or [Fabric](http://fabricmc.net) optimized Minecraft server.

Provide optimized `init.d` startup script.

You only need to simlink it into `/etc/init.d/`


## Prerequisites

You first need to setup a full user and group without any password.

`sudo groupadd minecraft`

`sudo useradd -g minecraft -m minecraft`

`sudo passwd -d minecraft`


## Softwares

Required software:
- bash
- tar
- wget
- ps
- screen
- pigz
- Perl
- Python
- Java (>= JRE 16)

Recommanded software:
- GraalVM in front of JRE


## Tips

You need to add a user/group (default to _minecraft_) prior to run script.

If you choose to not use _minecraft_ `user:group` you must edit script.

You can initiate memory folder simlinked to a `tmpfs` folder to speedup worlds processing ; only in case of enought ram :)

You can also simlink backups folder to another location to save sdcard.

All simlinks must have minecraft (default user) `user:group`.

All plugins stored to plugins folder must be named `name_version.jar` to be correctly processed by scripts.


## Logs

All server logs are stored in `/var/log/minecraft.log`.

World logs are stored in `worlds/[worlname]/logs/`.


## Notifications

If you define settings in `files/pushover.conf`, the server can use [Pushover](https://pushover.net/) notification system (Bash/Python scripting) automatically.


## Help commands

**Usage: bash minecraft help**


| Command | Description |
| ------ | ------ |
| download 'version' | download server |
| create 'world''port' | create world on port |
| status 'world' | get status of world |
| start 'world' | start world |
| stop 'world' | stop world |
| restart 'world' | restart world |
| sync 'world' | synchronize world |
| clean 'world' | cleanup world |
| backup 'world' | backup world |
| repair 'world' | repair world |
| enable 'world' | enable world |
| disable 'world' | disable world |
| op 'world' 'name' | add player as operator |
| deop 'world' 'name' | remove player from operators |
| kick 'world' 'name' | kick player |
| ban 'world' 'name' | ban player |
| unban 'world' 'name' | unban player |
| bs | backup server |
| send 'command' | send command |
| starts | start all worlds |
| stops | stop all worlds |
| restarts | restart all worlds |
| syncs | synchronize all worlds |
| cleans | cleanup all worlds |
| backups | backup all worlds |
| servers | list all server jar |
| worlds | list all worlds |
| -force | force command (must be first) |


## History

**2021.12.24**
- Christmass release :)
- Add 'mods' and 'plugins' files
- This is a fully functional optimized server under Fabric with good mods!

**2021.12.16**
- Code corrections and optimizations
- Add files/minecraft.conf to overrides some options 

**2021.12.14**
- Code corrections
- Translation corrections
- Add memory capping option
- Add force command switch
- Add world cleanup after server upgrade (Fabric)

**2021.12.08**
- Code corrections

**2021.11.18**
- Update of pi.sh script to add new raspberry pi models specifications
- Update from local source files

**2021.08.19**
- Add RAMDISK switch to disable memory disk usage
- Add support for Fabric server (PAPER vs FABRIC switch)
- Corrections allowing usage of Fabric or Paper

**2021.07.27**
- Updated server to use Paper 1.17.1 (changed downloaded url)
- Corrections on server jar rolling
- Ressources adjust from x32 vs x64 against memory allocation
- Raised to 2.3Go memory allocation on x32 and up to 4Go on x64 systems
- Add scripts localizations

**2021.05.25**
Documentation update
First package [release](/Kraoc/minecraft-linux-server/-/releases)

**2021-03-26**
Multiple bug fixes

**2021-03-25**
Initial commit, first public release
