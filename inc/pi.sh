#!/bin/bash
# LATEST: 2021-12-14
#
# Raspberry PI model & capacities detection
#

#
MODELPCB=0
MODEL=0
MODELNAME=0
REVISION=0
RAM=0
PI=0
CPU=0
ARCH=32

doCheckArch() {
    MACHINE_TYPE=`uname -m`
    if [ ${MACHINE_TYPE} == 'aarch64' ]; then
        ARCH=64
    else
        ARCH=32
    fi
}

doPiDetect() {

    CPU=$(cat /proc/cpuinfo | grep processor | wc -l)
    MODELPCB=$(cat /proc/cpuinfo | grep 'Revision' | awk '{print $3}')

    case $MODELPCB in

        '900092')
                MODEL='Pi Zero'
                MODELNAME='Pi Z'
                REVISION='1.2'
                RAM=512
                PI='Z'
            ;;

        '900093'|'920093')
                MODEL='Pi Zero'
                MODELNAME='Pi Z'
                REVISION='1.3'
                RAM=512
                PI='Z'
            ;;

        '9000C1')
                MODEL='Pi Zero W'
                MODELNAME='Pi ZW'
                REVISION='1.1'
                RAM=512
                PI='ZW'
            ;;

        '902120')
                MODEL='Pi Zero 2 W'
                MODELNAME='Pi Z2W'
                REVISION='1.0'
                RAM=512
                PI='Z2W'
            ;;

        '900061')
                MODEL='Compute Module'
                MODELNAME='CM'
                REVISION='1.1'
                RAM=512
                PI='CM'
            ;;

        '0011'|'0014')
                MODEL='Compute Module 1'
                MODELNAME='CM1'
                REVISION='1.0'
                RAM=512
                PI='CM1'
            ;;

        'a020a0'|'a220a0')
                MODEL='Compute Module 3'
                MODELNAME='CM3'
                REVISION='1.0'
                RAM=1024
                PI='CM3'
            ;;

        'a02100')
                MODEL='Compute Module 3+'
                MODELNAME='CM3+'
                REVISION='1.0'
                RAM=1024
                PI='CM3+'
            ;;

        'a03140')
                MODEL='Compute Module 4'
                MODELNAME='CM4'
                REVISION='1.0'
                RAM=1024
                PI='CM4'
            ;;

        'b03140')
                MODEL='Compute Module 4'
                MODELNAME='CM4'
                REVISION='1.0'
                RAM=2048
                PI='CM4'
            ;;

        'c03140')
                MODEL='Compute Module 4'
                MODELNAME='CM4'
                REVISION='1.0'
                RAM=4096
                PI='CM4'
            ;;

        'd03140')
                MODEL='Compute Module 4'
                MODELNAME='CM4'
                REVISION='1.0'
                RAM=8192
                PI='CM4'
            ;;

        '0007'|'0008'|'0009')
                MODEL='Pi A'
                MODELNAME='Pi A'
                REVISION='2.0'
                RAM=256
                PI='A'
            ;;

        '0012'|'0015')
                MODEL='Pi A+'
                MODELNAME='Pi A+'
                REVISION='1.1'
                RAM=256
                PI='A+'
            ;;

        '900021')
                MODEL='Pi A+'
                MODELNAME='Pi A'
                REVISION='1.1'
                RAM=512
                PI='A+'
            ;;

        '0002'|'0003')
                MODEL='Pi B'
                MODELNAME='Pi B'
                REVISION='1.0'
                RAM=256
                PI='B'
            ;;

        '0004'|'0005'|'0006')
                MODEL='Pi B'
                MODELNAME='Pi B'
                REVISION='2.0'
                RAM=256
                PI='B'
            ;;

        '000d'|'000e'|'000f')
                MODEL='Pi B'
                MODELNAME='Pi B'
                REVISION='2.0'
                RAM=512
                PI='B'
            ;;

        '0010')
                MODEL='Pi B+'
                MODELNAME='Pi B+'
                REVISION='1.0'
                RAM=512
                PI='B+'
            ;;

        '0013'|'900032')
                MODEL='Pi B+'
                MODELNAME='Pi B+'
                REVISION='1.2'
                RAM=512
                PI='B+'
            ;;

        'a01040')
                MODEL='Pi 2 Model B'
                MODELNAME='Pi 2B'
                REVISION='1.0'
                RAM=1024
                PI='2'
            ;;

        'a01041'|'a21041')
                MODEL='Pi 2 Model B'
                MODELNAME='Pi 2B'
                REVISION='1.1'
                RAM=1024
                PI='2'
            ;;

        'a22042'|'a02042')
                MODEL='Pi 2 Model B'
                MODELNAME='Pi 2B'
                REVISION='1.2'
                RAM=1024
                PI='2'
            ;;

        '9020e0')
                MODEL='Pi 3 Model A+'
                MODELNAME='Pi 3A+'
                REVISION='1.0'
                RAM=512
                PI='3+'
            ;;

        'a02082'|'a22082'|'a32082')
                MODEL='Pi 3 Model B'
                MODELNAME='Pi 3B'
                REVISION='1.2'
                RAM=1204
                PI='3'
            ;;

        'a22083')
                MODEL='Pi 3 Model B'
                MODELNAME='Pi 3B'
                REVISION='1.3'
                RAM=1024
                PI='3'
            ;;

        'a020d3')
                MODEL='Pi 3 Model B+'
                MODELNAME='Pi 3B+'
                REVISION='1.3'
                RAM=1204
                PI='3+'
            ;;

        'a03111')
                MODEL='Pi 4 Model B'
                MODELNAME='Pi 4B'
                REVISION='1.1'
                RAM=1024
                PI='4'
            ;;

        'b03111')
                MODEL='Pi 4 Model B'
                MODELNAME='Pi 4B'
                REVISION='1.1'
                RAM=2048
                PI='4'
            ;;

        'c03111')
                MODEL='Pi 4 Model B'
                MODELNAME='Pi 4B'
                REVISION='1.1'
                RAM=4096
                PI='4'
            ;;

        'b03112')
                MODEL='Pi 4 Model B'
                MODELNAME='Pi 4B'
                REVISION='1.2'
                RAM=2048
                PI='4'
            ;;

        'c03112')
                MODEL='Pi 4 Model B'
                MODELNAME='Pi 4B'
                REVISION='1.2'
                RAM=4096
                PI='4'
            ;;

        'b03114')
                MODEL='Pi 4 Model B'
                MODELNAME='Pi 4B'
                REVISION='1.4'
                RAM=2048
                PI='4'
            ;;

        'c03114')
                MODEL='Pi 4 Model B'
                MODELNAME='Pi 4B'
                REVISION='1.4'
                RAM=4096
                PI='4'
            ;;

        'd03114')
                MODEL='Pi 4 Model B'
                MODELNAME='Pi 4B'
                REVISION='1.4'
                RAM=8096
                PI='4'
            ;;

        'c03130')
                MODEL='Pi 400'
                MODELNAME='Pi 400'
                REVISION='1.0'
                RAM=4096
                PI='400'
            ;;

        *)
                MODEL=0
                REVISION=0
                RAM=0
                PI=0
            ;;

    esac
}
