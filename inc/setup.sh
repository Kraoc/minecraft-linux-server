#!/bin/bash
# LATEST: 2021-12-15
#
# SETUP
#

PROPERTIES_MOTD="MOTD"
PROPERTIES_SERVERPORT="SERVERPORT"
PROPERTIES_SERVERNAME="SERVERNAME"

# Initialize 'files' directory
doInitalizeFiles() {
    logWrite "$M_INITIALIZING_FILES"

    if [ ! -d $DIRECTORY/$FILESFOLDER ]; then
        mkdir -p $DIRECTORY/$FILESFOLDER
    fi

    if [ ! -d $DIRECTORY/$PLUGINSFOLDER ]; then
        mkdir -p $DIRECTORY/$PLUGINSFOLDER
    fi

    cd $DIRECTORY/$FILESFOLDER

    if [ ! -f banner.png ]; then
        ln -s ../$SETUPFOLDER/banner.png banner.png
    fi

    if [ ! -f server-icon.png ]; then
        ln -s ../$SETUPFOLDER/server-icon.png server-icon.png
    fi

    if [ ! -f server.properties ]; then
        cp ../$SETUPFOLDER/server.properties server.properties
    fi

    if [ ! -f paper.yml ]; then
        ln -s ../$SETUPFOLDER/paper.yml paper.yml
    fi

    if [ ! -f eula.txt ]; then
        ln -s ../$SETUPFOLDER/eula.txt eula.txt
    fi

    if [ ! -f debug.sh ]; then
        cp ../$SETUPFOLDER/debug.sh debug.sh
        chmod 0766 debug.sh
    fi

    if [ ! -f start.sh ]; then
        cp ../$SETUPFOLDER/start.sh start.sh
        chmod 0766 start.sh
    fi

    if [ ! -f stop.sh ]; then
        cp ../$SETUPFOLDER/stop.sh stop.sh
        chmod 0766 stop.sh
    fi

    if [ ! -f restart.sh ]; then
        cp ../$SETUPFOLDER/restart.sh restart.sh
        chmod 0766 restart.sh
    fi

    if [ ! -f sync.sh ]; then
        cp ../$SETUPFOLDER/sync.sh sync.sh
        chmod 0766 sync.sh
    fi

    if [ ! -f backup.sh ]; then
        cp ../$SETUPFOLDER/backup.sh backup.sh
        chmod 0766 backup.sh
    fi

    if [ ! -f pushover.py ]; then
        ln -s ../$SETUPFOLDER/pushover.py pushover.py
    fi

    if [ ! -f pushover.sh ]; then
        ln -s ../$SETUPFOLDER/pushover.sh pushover.sh
    fi

    if [ ! -f pushover.conf ]; then
        cp ../$SETUPFOLDER/pushover.conf pushover.conf
        chmod 0766 pushover.conf
    fi

    if [ ! -f minecraft ]; then
        cp ../$SETUPFOLDER/minecraft-init-d minecraft-init-d
        patchWorldScriptFile junk minecraft-init-d
        chmod 0744 minecraft-init-d
    fi

    if [ ! -f fabric-server-launcher.properties ]; then
        cp ../$SETUPFOLDER/fabric-server-launcher.properties fabric-server-launcher.properties
    fi

    cd $DIRECTORY

    if [ ! -f notify ]; then

        patchWorldScriptFile junk $FILESFOLDER/pushover.sh
        ln -s $FILESFOLDER/pushover.sh notify
        chown -R $USERNAME:$USERNAME notify

    fi

    chown -R $USERNAME:$USERNAME $FILESFOLDER/.
    chown -R $USERNAME:$USERNAME $PLUGINSFOLDER/.

    logWrite "$M_FILES_INITIALIZED"
}

# Download Paper server
doDownloadServerPaper() {
    version=$1
    paper=paper-$version.jar
    localf=/tmp/$paper

    doBuildRemoteUrl $version

    logWrite "$M_DOWNLOADING_SERVER"

    if [ $FORCECOMMAND -eq 1 ]; then
    	if [ -f $DIRECTORY/$FILESFOLDER/$paper ]; then
		rm -f $DIRECTORY/$FILESFOLDER/$paper
    	fi
    fi

    if [ ! -f $DIRECTORY/$FILESFOLDER/$paper ]; then

        wget --quiet --no-clobber --unlink --no-proxy --no-dns-cache --inet4-only --no-cache --no-check-certificate --recursive $REMOTEURL --output-document=$localf
        status=$?

        if [ $status -eq 0 ]; then

            mv $localf $DIRECTORY/$FILESFOLDER/$paper
            cd $DIRECTORY/$FILESFOLDER

            if [ -e server-latest.jar ]; then
                rm -f server-latest.jar
            fi

            ln -s $paper server-latest.jar
            cd $DIRECTORY
            chown -R $USERNAME:$USERNAME $DIRECTORY/$FILESFOLDER/.

            doPopulateServer

            logWrite "$M_SERVER_DOWNLOADED"

            serverJarList
            serverJarCleanup

            return 0 # 0 = true

        else
            rm -f $localf
            logWrite "$M_SERVER_DOWNLOAD_FAILURE"
            addError "$M_SERVER_DOWNLOAD_FAILURE"

            return 1 # 1 = false
        fi

    else
        logWrite "$M_SERVER_VERSION_ALREADY_DOWNLOADED"
        addError "$M_SERVER_VERSION_ALREADY_DOWNLOADED"

        return 1 # 1 = false
    fi
}

# Download Fabric server
doDownloadServerFabric() {
    version=$1
    fabricinstaller=fabric-installer-$version.jar
    localf=/tmp/$fabricinstaller

    doBuildRemoteUrl $version

    logWrite "$M_DOWNLOADING_INSTALLER"

    if [ $FORCECOMMAND -eq 1 ]; then
    	if [ -f $DIRECTORY/$FILESFOLDER/$fabricinstaller ]; then
		rm -f $DIRECTORY/$FILESFOLDER/$fabricinstaller
    	fi
        if [ -f $DIRECTORY/$FILESFOLDER/server.jar ]; then
                rm -rf $DIRECTORY/$FILESFOLDER/server.jar
        fi
    	if [ -d $DIRECTORY/$FILESFOLDER/libraries ]; then
		rm -rf $DIRECTORY/$FILESFOLDER/libraries
    	fi
    fi

    if [ ! -f $DIRECTORY/$FILESFOLDER/$fabricinstaller ]; then

        wget --quiet --no-clobber --unlink --no-proxy --no-dns-cache --inet4-only --no-cache --no-check-certificate --recursive $REMOTEURL --output-document=$localf
        status=$?

        if [ $status -eq 0 ]; then

            mv $localf $DIRECTORY/$FILESFOLDER/$fabricinstaller
            cd $DIRECTORY/$FILESFOLDER

            if [ -e installer-latest.jar ]; then
                rm -f installer-latest.jar
            fi

            ln -s $fabricinstaller installer-latest.jar
            fabric=fabric-server.jar
            localf=/tmp/$fabric
            logWrite "$M_DOWNLOADING_SERVER"

            cd $DIRECTORY/$FILESFOLDER

            logWrite "$M_GENERATING_SERVER"
            ${JAVA} -jar installer-latest.jar server -downloadMinecraft -dir $DIRECTORY/$FILESFOLDER/
            mv fabric-server-launch.jar $fabric

            if [ -e server-latest.jar ]; then
                rm -f server-latest.jar
            fi

            ln -s $fabric server-latest.jar
            cd $DIRECTORY
            chown -R $USERNAME:$USERNAME $DIRECTORY/$FILESFOLDER/.

            doPopulateServer

            logWrite "$M_SERVER_DOWNLOADED"

            serverJarList
            serverJarCleanup
            serverCleanupAllAfterUpgrade

            return 0 # 0 = true
        else
            rm -f $localf
            logWrite "$M_SERVER_DOWNLOAD_FAILURE"
            addError "$M_SERVER_DOWNLOAD_FAILURE"

            return 1 # 1 = false
        fi

    else
        logWrite "$M_SERVER_VERSION_ALREADY_DOWNLOADED"
        addError "$M_SERVER_VERSION_ALREADY_DOWNLOADED"

        return 1 # 1 = false
    fi
}

# Download server (Paper or Fabric)
doDownloadServer() {

    if [ $PAPER -eq 1 ]; then
        doDownloadServerPaper $1
    fi

    if [ $FABRIC -eq 1 ]; then
        doDownloadServerFabric $1
    fi

}

# Populate latest server
doPopulateServer() {
    cd $DIRECTORY/$FILESFOLDER

    if [ ! -L launcher.jar ]; then

        if [ -L server-latest.jar ]; then

            ln -s server-latest.jar launcher.jar
            logWrite "$M_SERVER_POPULATED"

            return 0 # 0 = true
        else
            logWrite "$M_NO_SERVER_DOWNLOADED"
            addError "$M_NO_SERVER_DOWNLOADED"

            return 1 # 1 = false
        fi

    fi

    chown -R $USERNAME:$USERNAME ../$FILESFOLDER/.
    cd $DIRECTORY
}
