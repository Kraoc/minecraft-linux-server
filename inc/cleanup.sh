#!/bin/bash
# LATEST: 2021-12-16
#
# CLEANUP
#

# Cleanup world logs
serverWorldLogsCleanup() {
    world=$1

    serverWorldExist $world
    status=$?

    if [ $status -eq 0 ]; then

        logWorldWrite $world "$M_LOGCLEANUP"
        folder=$WORLDSBASE/$world
        rm -rf $folder/logs/*.gz 1> /dev/null 2>&1

    else
        logWorldWrite $world "$M_DONT_EXIST"
        addErrorWorld $world "$M_DONT_EXIST"

        return 1 # 1 = false
    fi
}

# Cleanup a world
serverWorldCleanup() {
    world=$1

    serverWorldExist $world
    status=$?

    if [ $status -eq 0 ]; then

        logWorldWrite $world "$M_CLEANUP"
        serverWorldLogsCleanup $world
        folder=$WORLDSBASE/$world
        
        rm -rf $folder/.console_history 1> /dev/null 2>&1

    else
        logWorldWrite $world "$M_DONT_EXIST"
        addErrorWorld $world "$M_DONT_EXIST"

        return 1 # 1 = false
    fi
}

