#!/bin/bash
# LATEST: 2021-12-15
#
# MAIN
#

case "$1" in
    -force)
        FORCECOMMAND=1
        shift 1
    ;;
esac

action=$1

if [ "$action" != "download" ]; then
    if [ $NEEDSERVER -eq 1 ]; then
        consoleWrite "$M_NO_SERVER_FOUND"
        consoleWrite "$M_PLEASE_DO_DOWNLOAD"
        consoleWrite ""
        action='help'
    fi
fi

case "$action" in

    download)
        if [ $# -eq 2 ]; then
            version=$2
            consoleWrite "$M_DOWNLOADING_SERVER"
            doDownloadServer $version
            status=$?
            if [ $status -eq 0 ]; then
                consoleWrite "$M_DONE"
            else
                doPrintError
            fi
        else
            consoleWrite "$M_INVALID_ARGUMENTS"
        fi
    ;;

    create)
        if [ $# -eq 3 ]; then
            world=$2
            port=$3
            consoleWrite "$M_CREATING_WORLD"
            serverWorldCreate $world $port
            status=$?
            if [ $status -eq 0 ]; then
                consoleWrite "$M_DONE"
            else
                doPrintError
            fi
        else
            consoleWrite "$M_INVALID_ARGUMENTS"
        fi
    ;;

    repair)
        if [ $# -eq 2 ]; then
            world=$2
            if doRepair $world; then
                consoleWrite "$M_WORLD_REPAIRED"
            else
                consoleWrite "$M_UNABLE_TO_REPAIR_WORLD"
            fi
        else
            consoleWrite "$M_INVALID_ARGUMENTS"
        fi
    ;;

    clean)
        if [ $# -eq 2 ]; then
            world=$2
            if doCleanup $world; then
                consoleWrite "$M_WORLD_CLEANED"
            else
                consoleWrite "$M_UNABLE_TO_CLEAN_WORLD"
            fi
        else
            consoleWrite "$M_INVALID_ARGUMENTS"
        fi
    ;;

    enable)
        if [ $# -eq 2 ]; then
            world=$2
            consoleWrite "$M_ENABLING_WORLD"
            doEnable $world
            status=$?
            if [ $status -eq 0 ]; then
                consoleWrite "$M_DONE"
            else
                doPrintError
            fi
        else
            consoleWrite "$M_INVALID_ARGUMENTS"
        fi
    ;;

    disable)
        if [ $# -eq 2 ]; then
            world=$2
            consoleWrite "$M_DISABLING_WORLD"
            doDisable $world
            status=$?
            if [ $status -eq 0 ]; then
                consoleWrite "$M_DONE"
            else
                doPrintError
            fi
        else
            consoleWrite "$M_INVALID_ARGUMENTS"
        fi
    ;;

    refresh)
        if [ $# -eq 2 ]; then
            world=$2
            consoleWrite "$M_REFRESHING_WORLD"
            doRefresh $world
            status=$?
            if [ $status -eq 0 ]; then
                consoleWrite "$M_DONE"
            else
                doPrintError
            fi
        else
            consoleWrite "$M_INVALID_ARGUMENTS"
        fi
    ;;

    plugins)
        if [ $# -eq 2 ]; then
            world=$2
            consoleWrite "$M_REFRESHING_WORLD_PLUGINS"
            serverWorldUpdatePlugins $world
            status=$?
            if [ $status -eq 0 ]; then
                consoleWrite "$M_DONE"
            else
                doPrintError
            fi
        else
            consoleWrite "$M_INVALID_ARGUMENTS"
        fi
    ;;

    start)
        if [ $# -eq 2 ]; then
            world=$2
            consoleWrite "$M_STARTING_WORLD"
            doStart $world
            status=$?
            if [ $status -eq 0 ]; then
                consoleWrite "$M_DONE"
            else
                doPrintError
            fi
        else
            consoleWrite "$M_INVALID_ARGUMENTS"
        fi
    ;;

    stop)
        if [ $# -eq 2 ]; then
            world=$2
            consoleWrite "$M_STOPPING_WORLD"
            doStop $world
            status=$?
            if [ $status -eq 0 ]; then
                consoleWrite "$M_DONE"
            else
                doPrintError
            fi
        else
            consoleWrite "$M_INVALID_ARGUMENTS"
        fi
    ;;

    status)
        if [ $# -eq 2 ]; then
            world=$2
            if doStatus $world; then
                consoleWrite "$M_WORLD_RUNNING"
            else
                consoleWrite "$M_WORLD_NOT_RUNNING"
            fi
        else
            consoleWrite "$M_INVALID_ARGUMENTS"
        fi
    ;;

    restart)
        if [ $# -eq 2 ]; then
            world=$2
            consoleWrite "$M_RESTARTING_WORLD"
            doRestart $world
            status=$?
            if [ $status -eq 0 ]; then
                consoleWrite "$M_DONE"
            else
                doPrintError
            fi
        else
            consoleWrite "$M_INVALID_ARGUMENTS"
        fi
    ;;

    sync)
        if [ $# -eq 2 ]; then
            world=$2
            consoleWrite "$M_SYCHNRONIZING_WORLD"
            doSynchronize $world
            status=$?
            if [ $status -eq 0 ]; then
                consoleWrite "$M_DONE"
            else
                doPrintError
            fi
        else
            consoleWrite "$M_INVALID_ARGUMENTS"
        fi
    ;;

    bs)
        consoleWrite "$M_BACKING_UP_SERVER"
        doBackupServer
        status=$?
        if [ $status -eq 0 ]; then
            consoleWrite "$M_DONE"
        else
            doPrintError
        fi
    ;;

    backup)
        if [ $# -eq 2 ]; then
            world=$2
            consoleWrite "$M_BACKING_UP_WORLD"
            doBackupWorld $world
            status=$?
            if [ $status -eq 0 ]; then
                consoleWrite "$M_DONE"
            else
                doPrintError
            fi
        else
            consoleWrite "$M_INVALID_ARGUMENTS"
        fi
    ;;

    say)
        if [ $# -eq 3 ]; then
            world=$2
            message=$3
            doSay $world $message
            status=$?
            if [ $status -eq 0 ]; then
                consoleWrite "$M_DONE"
            else
                doPrintError
            fi
        else
            consoleWrite "$M_INVALID_ARGUMENTS"
        fi
    ;;

    send)
        if [ $# -gt 1 ]; then
            shift
            world=$2
            shift
            doCommand $world "$*"
        else
            consoleWrite "$M_MUST_SPECIFY_SERVER_COMMAND"
        fi
    ;;

    op)
        if [ $# -eq 3 ]; then
            world=$2
            user=$3
            consoleWrite "$M_ADDING_OPERATOR"
            doOp $world $user
            status=$?
            if [ $status -eq 0 ]; then
                consoleWrite "$M_DONE"
            else
                doPrintError
            fi
        else
            consoleWrite "$M_INVALID_ARGUMENTS"
        fi
    ;;

    deop)
        if [ $# -eq 3 ]; then
            world=$2
            user=$3
            consoleWrite "$M_REMOVING_OPERATOR"
            doDeop $world $user
            status=$?
            if [ $status -eq 0 ]; then
                consoleWrite "$M_DONE"
            else
                doPrintError
            fi
        else
            consoleWrite "$M_INVALID_ARGUMENTS"
        fi
    ;;

    kick)
        if [ $# -eq 3 ]; then
            world=$2
            user=$3
            consoleWrite "$M_KICKING_PLAYER"
            doKick $world $user
            status=$?
            if [ $status -eq 0 ]; then
                consoleWrite "$M_DONE"
            else
                doPrintError
            fi
        else
            consoleWrite "$M_INVALID_ARGUMENTS"
        fi
    ;;

    ban)
        if [ $# -eq 3 ]; then
            world=$2
            user=$3
            consoleWrite "$M_BAN_PLAYER"
            doBan $world $user
            status=$?
            if [ $status -eq 0 ]; then
                consoleWrite "$M_DONE"
            else
                doPrintError
            fi
        else
            consoleWrite "$M_INVALID_ARGUMENTS"
        fi
    ;;

    unban)
        if [ $# -eq 3 ]; then
            world=$2
            user=$3
            consoleWrite "$M_UNBAN_PLAYER"
            doUnban $world $user
            status=$?
            if [ $status -eq 0 ]; then
                consoleWrite "$M_DONE"
            else
                doPrintError
            fi
        else
            consoleWrite "$M_INVALID_ARGUMENTS"
        fi
    ;;

    worlds)
        consoleWrite "$M_LISTINGS_WORLDS"
        serverWorldList
        status=$?
        if [ $status -eq 0 ]; then
            serverWorldListDisplay
            consoleWrite "$M_DONE"
        else
            doPrintError
        fi
    ;;

    servers)
        consoleWrite "$M_LISTINGS_SERVER_JAR"
        serverJarList
        status=$?
        if [ $status -eq 0 ]; then
            serverJarListDisplay
            consoleWrite "$M_DONE"
        else
            doPrintError
        fi
    ;;

    starts)
        consoleWrite "$M_STARTING_ALL_WORLDS"
        doStartAll
        status=$?
        if [ $status -eq 0 ]; then
            consoleWrite "$M_DONE"
        else
            doPrintError
        fi
    ;;

    running)
        consoleWrite "$M_RUNNING_ALL_WORLDS"
        doRunningAll
        status=$?
        if [ $status -eq 0 ]; then
            consoleWrite "$M_DONE"
        else
            doPrintError
        fi
    ;;

    stops)
        consoleWrite "$M_STOPPING_ALL_WORLDS"
        doStopAll
        status=$?
        if [ $status -eq 0 ]; then
            consoleWrite "$M_DONE"
        else
            doPrintError
        fi
    ;;

    restarts)
        consoleWrite "$M_RESTARTING_ALL_WORLDS"
        doRestartAll
        status=$?
        if [ $status -eq 0 ]; then
            consoleWrite "$M_DONE"
        else
            doPrintError
        fi
    ;;

    syncs)
        consoleWrite "$M_SYCHNRONIZING_ALL_WORLDS"
        doSynchronizeAll
        status=$?
        if [ $status -eq 0 ]; then
            consoleWrite "$M_DONE"
        else
            doPrintError
        fi
    ;;

    cleans)
        consoleWrite "$M_CLEANING_ALL_WORLDS"
        doCleanupAll
        status=$?
        if [ $status -eq 0 ]; then
            consoleWrite "$M_DONE"
        else
            doPrintError
        fi
    ;;

    backups)
        consoleWrite "$M_BACKING_UP_ALL_WORLDS"
        doBackupAll
        status=$?
        if [ $status -eq 0 ]; then
            consoleWrite "$M_DONE"
        else
            doPrintError
        fi
    ;;

    help)
        consoleWrite "$M_USAGE"
    ;;

    *)
        consoleWrite "$M_NO_OR_UNKNOWN_COMMAND"
    ;;

esac
