#!/bin/bash
# LATEST: 2021-12-16
#
# PROPERTIES
#

patchWorldScriptFile() {
    world=$1
    file=$2
    script=$DIRECTORY/$SCRIPTNAME
    pushover=$DIRECTORY/$FILESFOLDER/pushover.py

    sed -i "s|\[BASH\]|$BASH|" $file
    sed -i "s|\[SCRIPT\]|$script|" $file
    sed -i "s|\[WORLD\]|$world|" $file
    sed -i "s|\[PYTHON\]|$PYTHON|" $file
    sed -i "s|\[PUSHOVER\]|$pushover|" $file
    sed -i "s|\[DIRECTORY\]|$DIRECTORY|" $file
}
patchWorldDebugFile() {
    world=$1
    file=$2

    patchWorldScriptFile $world $file

    OIFS=$IFS
    IFS=' '
    tmp_array=($JAVAOPTS)
    IFS=$OIFS
    JAVAOPTS_DBG=''
    tmp_size=${#tmp_array[@]}
    tmp_a=1

    for i in ${tmp_array[@]}; do
        JAVAOPTS_DBG="$JAVAOPTS_DBG$i"

        if [ "$tmp_a" -lt "$tmp_size" ]; then
            JAVAOPTS_DBG="$JAVAOPTS_DBG *"
            JAVAOPTS_DBG="$JAVAOPTS_DBG\\"$'\n'""
        fi

        tmp_a=$((tmp_a+1))
    done

    INVOCATION_DBG="$JAVA *\\
-server *\\
-Xms${MEMORYMIN}M *\\
-Xmx${MEMORYMAX}M *\\
$JAVAOPTS_DBG *\\
-jar $SERVER *\\
nogui"

    sed -i "s|\[INVOCATION\]|$INVOCATION_DBG|" $file
    sed -i "s|*|\\\|" $file
}
patchWorldProperties() {
    world=$1
    file=$2
    port=$3
    script=$DIRECTORY/$SCRIPTNAME
    date=$(date '+%Y-%m-%d %H:%M:%S')

    sed -i "s|\[SERVERNAME\]|$world|" $file
    sed -i "s|\[MOTD\]|$GAMETITLE\ ${world^}|" $file
    sed -i "s|\[SERVERPORT\]|$port|" $file
    sed -i "s|\[DATE\]|$date|" $file
}