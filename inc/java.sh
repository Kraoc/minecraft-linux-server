#!/bin/bash
# LATEST: 2021-12-15
#
# JAVA
#

# Memory default settings
MEMORYMIN=128
MEMORYMAX=384
NUMBERCPU=1

# Java server
SERVER=$SERVERBASE/launcher.jar

# Perform system ressources adjustments
doCheckArch
doPiDetect
doSystemAdjust

# Java invocation
INVOCATION="${JAVA} -server \
-Xms${MEMORYMIN}M \
-Xmx${MEMORYMAX}M \
${JAVAOPTS} \
-jar $SERVER \
nogui"
