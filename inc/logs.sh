#!/bin/bash
# LATEST: 2021-12-15
#
# LOGS
#

#
LOGS=$LOGBASE/$GAME.log

# Check global log file
logExist() {
    if [ -f $LOGS ]; then
        return 0 # 0 = true
    else
        return 1 # 1 = false
    fi
}

# Set global log
logSet() {
    if [ ! -d $LOGBASE ]; then
        mkdir -p $LOGBASE
        chown -R $USERNAME:$USERNAME $LOGBASE
        chmod 0777 $LOGBASE
    fi

    logExist
    status=$?

    if [ $status -eq 1 ]; then

        touch $LOGS
        chown -R $USERNAME:$USERNAME $LOGS
        chmod 0644 $LOGS

    fi
}

# Unset global log
logUnset() {
    logExist
    status=$?

    if [ $status -eq 0 ]; then
        rm -f $LOGS
    fi
}

# Unset global log
logTruncate() {
    logUnset
    logSet
    status=$?

    if [ $status -eq 0 ]; then
        return 0 # 0 = true
    else
        return 1 # 1 = false
    fi
}

# Write to logs
logWrite() {
    logExist
    status=$?

    if [ $status -eq 1 ]; then
        logSet
    fi

    message=$1
        m1=$message
        m1=${m1/T_WORLD/$world}
        m2=${m1/T_ERROR/$ERROR}
        m3=${m2/T_LOGS/$LOGS}
        m4=${m3/T_PID/$PID}
        m5=${m4/T_COMMAND/$command}
        message=$m5

    NOW=`date "+%Y-%m-%d %H:%M:%S"`
    echo "$NOW : $message" >> $LOGS
}

# Write world message to logs
logWorldWrite() {
    world=$1
    message=$2
    logWrite "[$world] $message"
}
