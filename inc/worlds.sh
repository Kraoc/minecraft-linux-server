#!/bin/bash
# LATEST: 2021-12-19
#
# WORLDS
#

declare -a WORLDSLIST

. $DIRECTORY/inc/plugins.sh
. $DIRECTORY/inc/cleanup.sh
. $DIRECTORY/inc/properties.sh

# Check if world created
serverWorldExist() {
    world=$1
    folder=$WORLDSBASE/$world

    if [ -d $folder ]; then
        logWorldWrite $world "$M_FOUND"

        return 0 # 0 = true
    else
        logWorldWrite $world "$M_NOT_FOUND"

        return 1 # 1 = false
    fi
}

# Create a world
serverWorldCreate() {
    world=$1
    port=$2
    folder=$WORLDSBASE/$world

    if [ ! -d $folder ]; then

        # create fodler
        logWorldWrite $world ">>> $M_CREATING"
        logWorldWrite $world "$M_CREATE_FOLDER"
        mkdir -p $folder
        chown $USERNAME:$USERNAME $WORLDSBASE
        cd $folder

        # link files
        logWorldWrite $world "$M_LINK_FILES"
        ln -s ../../$FILESFOLDER/eula.txt
        ln -s ../../$FILESFOLDER/server-icon.png
        ln -s ../../$FILESFOLDER/banner.png
        ln -s ../../$FILESFOLDER/bukkit.yml
        ln -s ../../$FILESFOLDER/spigot.yml
        ln -s ../../$FILESFOLDER/paper.yml
        ln -s ../../$FILESFOLDER/fabric-server-launcher.properties

        # copy files
        logWorldWrite $world "$M_COPY_FILES"
        cp -P ../../$FILESFOLDER/server.properties .
        cp -P ../../$FILESFOLDER/debug.sh .
        cp -P ../../$FILESFOLDER/sync.sh .
        cp -P ../../$FILESFOLDER/backup.sh .
        cp -P ../../$FILESFOLDER/start.sh .
        cp -P ../../$FILESFOLDER/stop.sh .
        cp -P ../../$FILESFOLDER/restart.sh .

        # make sub directories
        logWorldWrite $world "$M_CREATE_DIRECTORIES"
        mkdir -p cache
        mkdir -p logs
        mkdir -p plugins
        mkdir -p mods
        mkdir -p world
        mkdir -p world_nether
        mkdir -p world_the_end

        # patch files
        logWorldWrite $world "$M_PATCH_FILES"
        patchWorldDebugFile  $world debug.sh
        patchWorldScriptFile $world start.sh
        patchWorldScriptFile $world stop.sh
        patchWorldScriptFile $world restart.sh
        patchWorldScriptFile $world sync.sh
        patchWorldScriptFile $world backup.sh
        patchWorldProperties $world server.properties $port
        patchWorldProperties $world fabric-server-launcher.properties

        # populate plugins
        serverWorldUpdatePlugins $world
        serverWorldUpdateMods $world

        # setup rights
        logWorldWrite $world "$M_SETUP_RIGHTS"
        cd $DIRECTORY
        chown -R $USERNAME:$USERNAME $folder
        chmod 0755 $folder
        logWorldWrite $world "$M_CREATED"

        return 0 # 0 = true
    else
        logWorldWrite $world "$M_ALREADY_EXIST"
        addErrorWorld $world "$M_ALREADY_EXIST"

        return 1 # 1 = false
    fi
}

# Start a world server
serverWorldStart() {
    world=$1

    serverWorldExist $world
    status=$?

    if [ $status -eq 0 ]; then

        isWorldRunning $world
        status=$?

        if [ $status -eq 1 ]; then

            logWorldWrite $world ">>> $M_STARTING"
            doNotify "$M_WORLD_STARTING"

            serverWorldCleanup $world

            if [ $PAPER -eq 1 ]; then
                serverWorldUpdatePlugins $world
            fi
            if [ $FABRIC -eq 1 ]; then
                serverWorldUpdateMods $world
            fi
            status=$?

            if [ $status -eq 0 ]; then

                SNW=$SCREENNAME$world

                # create memory
                memoryCreate $world
                status=$?

                if [ $status -eq 0 ]; then

                    # execute world server invocation
                    execAsUser "cd $WORLDSBASE/$world && $SCREEN -h $SCREENHISTORY -dmS $SNW $INVOCATION world=$world"
                    sleep 1

                    # wait until server start
                    while ! isWorldRunning $world; do
                       sleep 1
                    done

                    pidWorldSet $world
                    logWorldWrite $world "$M_RUNNING"
                    doNotify "$M_WORLD_RUNNING"

                    return 0 # 0 = true
                else
                    logWorldWrite $world "$M_MEMORY_CREATION_FAILURE"
                    addErrorWorld $world "$M_MEMORY_CREATION_FAILURE"

                    return 1 # 1 = false
                fi

            else
                logWorldWrite $world "$M_PID_CREATION_FAILURE"
                addErrorWorld $world "$M_PID_CREATION_FAILURE"

                return 1 # 1 = false
            fi

        else
            logWorldWrite $world "$M_ALREADY_RUNNING"
            addErrorWorld $world "$M_ALREADY_RUNNING"

            return 1 # 1 = false
        fi

    else
        logWorldWrite $world "$M_DONT_EXIST"
        addErrorWorld $world "$M_DONT_EXIST"
        return 1 # 1 = false
    fi
}

# Stop a world server
serverWorldStop() {
    world=$1

    serverWorldCheckConsistency $world
    status=$?

    if [ $status -eq 0 ]; then

        serverWorldExist $world
        status=$?

        if [ $status -eq 0 ]; then

            isWorldRunning $world
            status=$?

            if [ $status -eq 0 ]; then

                logWorldWrite $world ">>> $M_STOPPING"

                # communicate stopping
                doSay $world "$M_SHUTTING_DOWN"
                doNotify "$M_WORLD_STOPPING"
                doSave_All $world
                sync

                # send stop commands
                doCommand $world "stop"
                doCommand $world "end"

                # wait until server stop
                while isWorldRunning $world; do
                   sleep 1
                done

                # stop memory
                memoryDelete $world
                status=$?

                if [ $status -eq 0 ]; then

                    pidWorldUnset $world
                    logWorldWrite $world "$M_STOPPED"
                    doNotify "$M_WORLD_STOPPED"
                    doFreeMemory

                    return 0 # 0 = true
                else
                    logWorldWrite $world "$M_STOPPING_FAILURE"
                    addErrorWorld $world "$M_STOPPING_FAILURE"

                    return 1 # 1 = false
                fi
            else
                logWorldWrite $world "$M_NOT_RUNNING"
                addErrorWorld $world "$M_NOT_RUNNING"

                return 1 # 1 = false
            fi
        else
            logWorldWrite $world "$M_DONT_EXIST"
            addErrorWorld $world "$M_DONT_EXIST"

            return 1 # 1 = false
        fi
    else
        return 1 # 1 = false
    fi
}

# List enabled worlds
serverWorldList() {
    WORLDSLIST=()
    i=1

    for world in $WORLDSBASE/*
    do
        world="${world%/}"
        world=$( basename $world )

        if [[ ! -z "$world" && "$world" != "*" ]]; then
            if [ ! -f $WORLDSBASE/$world/disabled ]; then
            	WORLDSLIST[i++]=$world
            fi
        fi
    done

    if [ ${#WORLDSLIST[@]} -gt 0 ]; then
        return 0 # 0 = true
    else
        logWrite "$M_NO_WORLD_FOUND"
        addError "$M_NO_WORLD_FOUND"

        return 1 # 1 = false
    fi
}

# List all worlds
serverWorldListAll() {
    WORLDSLIST=()
    i=1

    for world in $WORLDSBASE/*
    do
        world="${world%/}"
        world=$( basename $world )

        if [[ ! -z "$world" && "$world" != "*" ]]; then
            WORLDSLIST[i++]=$world
        fi
    done

    if [ ${#WORLDSLIST[@]} -gt 0 ]; then
        return 0 # 0 = true
    else
        logWrite "$M_NO_WORLD_FOUND"
        addError "$M_NO_WORLD_FOUND"

        return 1 # 1 = false
    fi
}


# List all worlds
serverWorldListDisplay() {
    for ((i=1; i<=${#WORLDSLIST[@]}; i++))
    do
        consoleWrite "* ${WORLDSLIST[i]}"
    done
}

# Check world consistency
serverWorldCheckConsistency() {
    world=$1

    serverWorldExist $world
    status=$?

    if [ $status -eq 0 ]; then

        logWorldWrite $world "$M_CHECKCONSISTENCY"

        # check world lock
        lockWorldExist $world
        worldLockExist=$?

        # check memory lock
        memoryWorldLockExist $world
        memoryLockExist=$?

        # check world pid
        pidWorldExist $world
        pidExist=$?

        # check world running
        isWorldRunning $world
        worldRunning=$?

        # check consistency
        if [[ $memoryLockExist -eq 0 && $worldRunning -eq 1 ]]; then
            logWorldWrite $world "$M_CRASHED_NEED_TO_REPAIR"
            addErrorWorld $world "$M_CRASHED_NEED_TO_REPAIR"

            return 1 # 1 = false
        else
            return 0 # 0 = true
        fi

    else
        logWorldWrite $world "$M_DONT_EXIST"
        addErrorWorld $world "$M_DONT_EXIST"

        return 1 # 1 = false
    fi
}

# Repair a world
serverWorldRepair() {
    world=$1

    serverWorldCheckConsistency $world
    status=$?

    if [ $status -eq 1 ]; then

        logWorldWrite $world ">>> $M_REPAIR"

        memoryRepair $world
        status=$?

        if [ $status -eq 0 ]; then
            logWorldWrite $world "$M_OK_WORLD_RECOVERED_FROM_MEMORY"

            return 0 # 0 = true
        else

            syncRepair $world
            status=$?

            if [ $status -eq 0 ]; then
                logWorldWrite $world "$M_OK_WORLD_RECOVERED_FROM_SYNC"

                return 0 # 0 = true
            else
                logWorldWrite $world "$M_UNABLE_TO_REPAIR_FROM_MEMORY_OR_SYNC"
                addErrorWorld $world "$M_UNABLE_TO_REPAIR_FROM_MEMORY_OR_SYNC"

                return 1 # 1 = false
            fi
        fi
    else
        logWorldWrite $world "$M_OK_NO_NEED_TO_REPAIR"
        addErrorWorld $world "$M_OK_NO_NEED_TO_REPAIR"

        return 0 # 0 = true
    fi
}

# Check world disabled file
serverWorldDisableExist() {
    world=$1
    folder=$WORLDSBASE/$world
    disabled=$folder/disabled

    if [ -f $disabled ]; then
        return 0 # 0 = true
    else
        return 1 # 1 = false
    fi
}

# World Disable
serverWorldDisable() {
    world=$1

    serverWorldExist $world
    status=$?

    if [ $status -eq 0 ]; then

        logWorldWrite $world ">>> $M_DISABLING_WORLD"

        isWorldRunning $world
        status=$?

        if [ $status -eq 1 ]; then

            serverWorldDisableExist $world
            status=$?

            if [ $status -eq 1 ]; then

                doNotify "$M_WORLD_DISABLING"
                touch $WORLDSBASE/$world/disabled
                chmod 0644 $WORLDSBASE/$world/disabled
                chown $USERNAME:$USERNAME $WORLDSBASE/$world/disabled

            else
                logWorldWrite $world "$M_IS_ALREADY_DISABLED"
                addErrorWorld $world "$M_IS_ALREADY_DISABLED"

                return 1 # 1 = false
            fi
        else
            logWorldWrite $world "$M_IS_RUNNING"
            addErrorWorld $world "$M_IS_RUNNING"

            return 1 # 1 = false
        fi
    else
        logWorldWrite $world "$M_DONT_EXIST"
        addErrorWorld $world "$M_DONT_EXIST"

        return 1 # 1 = false
    fi
}

# World Enable
serverWorldEnable() {
    world=$1

    serverWorldExist $world
    status=$?

    if [ $status -eq 0 ]; then
        logWorldWrite $world ">>> $M_ENABLING_WORLD"

        isWorldRunning $world
        status=$?

        if [ $status -eq 1 ]; then

            serverWorldDisableExist $world
            status=$?

            if [ $status -eq 0 ]; then

                doNotify "$M_WORLD_ENABLING"
                rm -f $WORLDSBASE/$world/disabled

            else
                logWorldWrite $world "$M_IS_ALREADY_ENABLED"
                addErrorWorld $world "$M_IS_ALREADY_ENABLED"

                return 1 # 1 = false
            fi
        else
            logWorldWrite $world "$M_IS_RUNNING"
            addErrorWorld $world "$M_IS_RUNNING"

            return 1 # 1 = false
        fi
    else
        logWorldWrite $world "$M_DONT_EXIST"
        addErrorWorld $world "$M_DONT_EXIST"

        return 1 # 1 = false
    fi
}

# World refresh files
serverWorldRefreshFiles() {
    world=$1
    port=$2
    folder=$WORLDSBASE/$world

    if [ -d $folder ]; then

        # create fodler
        logWorldWrite $world ">>> $M_REFRESHING"
        chown $USERNAME:$USERNAME $WORLDSBASE
        cd $folder

        # link files
        logWorldWrite $world "$M_LINK_FILES"
        rm -f eula.txt && ln -s ../../$FILESFOLDER/eula.txt
        rm -f server-icon.png && ln -s ../../$FILESFOLDER/server-icon.png
        rm -f banner.png && ln -s ../../$FILESFOLDER/banner.png
        rm -f bukkit.yml && ln -s ../../$FILESFOLDER/bukkit.yml
        rm -f spigot.yml && ln -s ../../$FILESFOLDER/spigot.yml
        rm -f paper.yml && ln -s ../../$FILESFOLDER/paper.yml
        rm -f fabric-server-launcher.properties && ln -s ../../$FILESFOLDER/fabric-server-launcher.properties

        # copy files
        logWorldWrite $world "$M_COPY_FILES"
        rm -f debug.sh && cp -P ../../$FILESFOLDER/debug.sh .
        rm -f sync.sh && cp -P ../../$FILESFOLDER/sync.sh .
        rm -f backup.sh && cp -P ../../$FILESFOLDER/backup.sh .
        rm -f start.sh && cp -P ../../$FILESFOLDER/start.sh .
        rm -f stop.sh && cp -P ../../$FILESFOLDER/stop.sh .
        rm -f restart.sh && cp -P ../../$FILESFOLDER/restart.sh .

        # patch files
        logWorldWrite $world "$M_PATCH_FILES"
        patchWorldDebugFile  $world debug.sh
        patchWorldScriptFile $world start.sh
        patchWorldScriptFile $world stop.sh
        patchWorldScriptFile $world restart.sh
        patchWorldScriptFile $world sync.sh
        patchWorldScriptFile $world backup.sh
        patchWorldProperties $world fabric-server-launcher.properties

        # populate plugins
        serverWorldUpdatePlugins $world
        serverWorldUpdateMods $world

        # setup rights
        logWorldWrite $world "$M_SETUP_RIGHTS"
        cd $DIRECTORY
        chown -R $USERNAME:$USERNAME $folder
        chmod 0755 $folder
        logWorldWrite $world "$M_REFRESHED"

        return 0 # 0 = true
    else
        logWorldWrite $world "$M_NO_WORLD_FOUND"
        addErrorWorld $world "$M_NO_WORLD_FOUND"

        return 1 # 1 = false
    fi
}

# World cleanup after a server upgrade
serverWorldCleanupAfterUpdate() {
    world=$1
    folder=$WORLDSBASE/$world

    if [ -d $folder ]; then

        logWorldWrite $world "$M_CLEANUP"

        if [ $FABRIC -eq 1 ]; then
            if [ -d $folder/.fabric ]; then
                rm -rf $folder/.fabric
            fi
            if [ -d $folder/libraries ]; then
                rm -rf $folder/libraries
            fi
            if [ -d $folder/versions ]; then
                rm -rf $folder/versions
            fi
        fi

        serverWorldCleanup $world

        return 0 # 0 = true
    else
        logWorldWrite $world "$M_NO_WORLD_FOUND"
        addErrorWorld $world "$M_NO_WORLD_FOUND"

        return 1 # 1 = false
    fi
}
