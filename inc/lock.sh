#!/bin/bash
# LATEST: 2021-12-15
#
# LOCK
#

#
LOCKIDBASE=$LOCKBASE/$GAME

# Check global lock file
lockExist() {
    if [ ! -d $LOCKIDBASE ]; then
        mkdir -p $LOCKIDBASE
        chown -R $USERNAME:$USERNAME $LOCKIDBASE
    fi

    lockid=$LOCKIDBASE/$GAME.lock

    if [ -f $lockid ]; then
        return 0 # 0 = true
    else
        return 1 # 1 = false
    fi
}

# Set global lock
lockSet() {
    lockExist
    status=$?

    if [ $status -eq 1 ]; then

        lockid=$LOCKIDBASE/$GAME.lock
        touch $lockid
        chown -R $USERNAME:$USERNAME $lockid

        return 0 # 0 = true
    else
        addError "$M_LOCK_SET_FAILURE"

        return 1 # 1 = false
    fi
}

# Unset global lock
lockUnset() {
    lockExist $world
    status=$?

    if [ $status -eq 0 ]; then

        lockid=$LOCKIDBASE/$GAME.lock
        rm -f $lockid
        return 0 # 0 = true
    else
        addError "$M_LOCK_UNSET_FAILURE"

        return 1 # 1 = false
    fi
}

# Check world lock file
lockWorldExist() {
    if [ ! -d $LOCKIDBASE ]; then
        mkdir -p $LOCKIDBASE
        chown -R $USERNAME:$USERNAME $LOCKIDBASE
    fi

    world=$1
    lockid=$LOCKIDBASE/$world.lock

    if [ -f $lockid ]; then
        return 0 # 0 = true
    else
        return 1 # 1 = false
    fi
}

# Set world lock
lockWorldSet() {
    world=$1

    lockWorldExist $world
    status=$?

    if [ $status -eq 1 ]; then

        lockid=$LOCKIDBASE/$world.lock
        touch $lockid
        chown -R $USERNAME:$USERNAME $lockid

        return 0 # 0 = true
    else
        addErrorWorld $world "$M_LOCK_SET_FAILURE"

        return 1 # 1 = false
    fi
}

# Unset world lock
lockWorldUnset() {
    world=$1

    lockWorldExist $world
    status=$?

    if [ $status -eq 0 ]; then
    
        lockid=$LOCKIDBASE/$world.lock
        rm -f $lockid

        return 0 # 0 = true
    else
        addErrorWorld $world "$M_LOCK_UNSET_FAILURE"

        return 1 # 1 = false
    fi
}
