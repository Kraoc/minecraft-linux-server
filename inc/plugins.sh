#!/bin/bash
# LATEST: 2021-12-19
#
# PLUGINS
#

# Update world plugins (Paper)
serverWorldUpdatePlugins() {
    if [ $PAPER -eq 1 ]; then
        world=$1

        serverWorldExist $world
        status=$?

        if [ $status -eq 0 ]; then

            folder=$WORLDSBASE/$world
            logWorldWrite $world "$M_REFRESH_PLUGINS"

            if [ -d $folder ]; then

                if [ ! -d $folder/plugins ]; then
                    mkdir -p $folder/plugins
                fi
		chown -R $USERNAME:$USERNAME $DIRECTORY/plugins
		chmod 0755 $DIRECTORY/plugins
		chmod 0644 $DIRECTORY/plugins/*.*

                cd $folder/plugins

                # remove old files
                rm -f *.jar

                # process plugins
                if [ "$(ls -A ../../../$PLUGINSFOLDER/)" ]; then

                    FILES=../../../$PLUGINSFOLDER/*.jar
                    for file in $FILES
                    do
                        f1=${file##*/}
                        f2=$(echo $f1 | cut -f1 -d"_")
                        ln -s $(realpath ../../../$PLUGINSFOLDER/${file##*/}) $f2.jar
                    done

                fi

                cd ..
                logWorldWrite $world "$M_PLUGINS_REFRESHED"

                return 0 # 0 = true
            else
                logWorldWrite $world "$M_DONT_EXIST"
                addErrorWorld $world "$M_DONT_EXIST"

                return 1 # 1 = false
            fi

        else
            return 1 # 1 = false
        fi
    else
        return 0 # 0 = true
    fi
}

# Update world mods (Fabric)
serverWorldUpdateMods() {
    if [ $FABRIC -eq 1 ]; then
        world=$1

        serverWorldExist $world
        status=$?

        if [ $status -eq 0 ]; then
            folder=$WORLDSBASE/$world
            logWorldWrite $world "$M_REFRESH_PLUGINS"

            if [ -d $folder ]; then

                if [ ! -d $folder/mods ]; then
                    mkdir -p $folder/mods
                fi
                chown -R $USERNAME:$USERNAME $DIRECTORY/mods
                chmod 0755 $DIRECTORY/mods
                chmod 0644 $DIRECTORY/mods/*.*

                cd $folder/mods

                # remove old files
                rm -f *.jar

                # process plugins
                if [ "$(ls -A ../../../$MODSFOLDER/)" ]; then
                    FILES=../../../$MODSFOLDER/*.jar
                    for file in $FILES
                    do
                        f1=${file##*/}
                        f2=$(echo $f1 | cut -f1 -d"_")
                        ln -s $(realpath ../../../$MODSFOLDER/${file##*/}) $f2.jar
                    done
                fi

                cd ..
                logWorldWrite $world "$M_PLUGINS_REFRESHED"

                return 0 # 0 = true
            else
                logWorldWrite $world "$M_DONT_EXIST"
                addErrorWorld $world "$M_DONT_EXIST"

                return 1 # 1 = false
            fi

        else
            return 1 # 1 = false
        fi
    else
        return 0 # 0 = true
    fi
}
