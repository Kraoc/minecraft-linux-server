#!/bin/bash
# LATEST: 2021-12-15
#
# CONSOLE
#

# Write to logs
consoleWrite() {
    message=$1
    
    m1=$message
    m1=${m1/T_WORLD/$world}
    m2=${m1/T_ERROR/$ERROR}
    m3=${m2/T_LOGS/$LOGS}
    m4=${m3/T_PID/$PID}
    m5=${m4/T_COMMAND/$command}
    message=$m5

    echo "$message"
}
