#!/bin/bash
# LATEST: 2021-12-15
#
# PID
#

PIDIDBASE=$PIDBASE/$GAME

# Get world process PID
getWorldProcessPID() {
    world=$1
    PID=$(
            $PS -a -U $USERNAME -o pid,comm,args -ww |
            $PERL -ne 'if ($_ =~ /^\s*(\d+)\s+java.+world='$world'$/) { print $1; }'
        )
    printf "%d\n" $PID
}

# Check world pid file
pidWorldExist() {
    if [ ! -d $PIDIDBASE ]; then
        mkdir -p $PIDIDBASE
        chown -R $USERNAME:$USERNAME $PIDIDBASE
    fi

    world=$1
    pidid=$PIDIDBASE/world.$world.pid

    if [ -f $pidid ]; then
        return 0 # 0 = true
    else
        return 1 # 1 = false
    fi
}

# Set world pid
pidWorldSet() {
    world=$1
    pidid=$PIDIDBASE/world.$world.pid

    pidWorldExist $world
    status=$?

    if [ $status -eq 1 ]; then

        touch $pidid
        PID=$(getWorldProcessPID $world)
        echo $PID > $pidid
        chown -R $USERNAME:$USERNAME $pidid

        logWorldWrite $world "$M_PID_CREATED"

        return 0 # 0 = true
    else
        addErrorWorld $world "$M_PID_SET_FAILURE"

        return 1 # 1 = false
    fi
}

# Unset world pid
pidWorldUnset() {
    world=$1
    pidid=$PIDIDBASE/world.$world.pid

    pidWorldExist $world
    status=$?

    if [ $status -eq 0 ]; then

        rm -f $pidid
        logWorldWrite $world "$M_PID_REMOVED"

        return 0 # 0 = true
    else
        addErrorWorld $world "$M_PID_UNSET_FAILURE"

        return 1 # 1 = false
    fi
}

# Check memory pid file
pidMemoryExist() {
    world=$1
    pidid=$PIDIDBASE/memory.$world.pid

    if [ -f $pidid ]; then
        return 0 # 0 = true
    else
        return 1 # 1 = false
    fi
}

# Set memory pid
pidMemorySet() {
    world=$1
    pidid=$PIDIDBASE/memory.$world.pid

    pidMemoryExist $world
    status=$?

    if [ $status -eq 1 ]; then

        touch $pidid
        chown -R $USERNAME:$USERNAME $pidid
        logWorldWrite $world "$M_MEMORY_PID_CREATED"

        return 0 # 0 = true
    else
        addErrorWorld $world "$M_MEMORY_PID_SET_FAILURE"

        return 1 # 1 = false
    fi
}

# Unset memory pid
pidMemoryUnset() {
    world=$1
    pidid=$PIDIDBASE/memory.$world.pid

    pidMemoryExist $world
    status=$?

    if [ $status -eq 0 ]; then

        rm -f $pidid
        logWorldWrite $world "$M_MEMORY_PID_REMOVED"

        return 0 # 0 = true
    else
        addErrorWorld $world "$M_MEMORY_PID_UNSET_FAILURE"

        return 1 # 1 = false
    fi
}

# Is world running
isWorldRunning() {
    world=$1
    
    pid=$(getWorldProcessPID $world)
    
    if [ $pid -gt 0 ]; then
        return 0 # 0 = true
    else
        return 1 # 1 = false
    fi
}
