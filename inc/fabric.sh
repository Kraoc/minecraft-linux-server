#!/bin/bash
# LATEST: 2021-12-14
#
# FABRIC
#

# compute paper server jar download url
doBuildRemoteUrl() {
    version=$1

    REMOTEURL=https://maven.fabricmc.net/net/fabricmc/fabric-installer/$version/fabric-installer-$version.jar
}
