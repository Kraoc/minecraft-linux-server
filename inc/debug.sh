#!/bin/bash
# LATEST: 2021-12-14
#
# DEBUG
#

# Print debug informations
printDebug() {
    consoleWrite '* PI'
    consoleWrite "      MODELPCB: $MODELPCB"
    consoleWrite "      MODEL: $MODEL"
    consoleWrite "      REVISION: $REVISION"
    consoleWrite "      RAM: $RAM"
    consoleWrite "      PI: $PI"
    consoleWrite "      ARCH: $ARCH bits"
    consoleWrite ''

    consoleWrite '* BASE'
    consoleWrite "      DIRECTORY: $DIRECTORY"
    consoleWrite ''

    consoleWrite '* SCRIPTS'
    consoleWrite "      SCRIPTEXT: $SCRIPTEXT"
    consoleWrite "      SCRIPTNAME: $SCRIPTNAME"
    consoleWrite ''

    consoleWrite '* USERS'
    consoleWrite "      USER: $USER"
    consoleWrite "      USERID: $USERID"
    consoleWrite "      USERNAME: $USERNAME"
    consoleWrite ''

    consoleWrite '* APPLICATIONS'
    consoleWrite "      TAR: $TAR"
    consoleWrite "      JAVA: $JAVA"
    consoleWrite "      PERL: $PERL"
    consoleWrite "      WGET: $WGET"
    consoleWrite "      ZIP: $ZIP"
    consoleWrite ''

    consoleWrite '* FOLDER BASE'
    consoleWrite "      MEMORYBASE: $MEMORYBASE"
    consoleWrite "      SYNCBASE: $SYNCBASE"
    consoleWrite "      WORLDSBASE: $WORLDSBASE"
    consoleWrite "      SERVERBASE: $SERVERBASE"
    consoleWrite "      LOCKIDBASE: $LOCKIDBASE"
    consoleWrite "      PIDIDBASE: $PIDIDBASE"
    consoleWrite "      BACKUPBASE: $BACKUPBASE"
    consoleWrite ''

    consoleWrite '* FILES'
    consoleWrite "      LOGS: $LOGS"
    consoleWrite ''

    consoleWrite '* JAVA'
    consoleWrite "      NUMBERCPU: $NUMBERCPU"
    consoleWrite "      MEMORYMIN: $MEMORYMIN"
    consoleWrite "      MEMORYMAX: $MEMORYMAX"
    consoleWrite "      SERVER: $SERVER"
    consoleWrite "      JAVAOPTS: $JAVAOPTS"
    consoleWrite "      INVOCATION: $INVOCATION"
    consoleWrite ''
}
