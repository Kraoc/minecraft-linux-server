#!/bin/bash
# LATEST: 2021-12-16
#
# SERVER
#

declare -a JARLIST

# Status
doStatus() {
    world=$1

    serverWorldCheckConsistency $world
    status=$?

    if [ $status -eq 0 ]; then

        isWorldRunning $world
        status=$?

        return $status
    else
        return 1 # 1 = false
    fi
}

# Start
doStart() {
    world=$1

    serverWorldStart $world
    status=$?

    return $status
}

# Stop
doStop() {
    world=$1

    serverWorldStop $world
    status=$?

    return $status
}

# Restart
doRestart() {
    world=$1

    doStop $world
    status=$?

    if [ $status -eq 0 ]; then

        doStart $world
        status=$?

        return $status # 0 = true # 1 = false
    else
        return 1 # 1 = false
    fi
}

# Repair
doRepair() {
    world=$1

    serverWorldRepair $world
    status=$?

    return $status
}

# Cleanup
doCleanup() {
    world=$1

    serverWorldLogsCleanup $world
    status=$?

    return $status
}

# Synchronize
doSynchronize() {
    world=$1

    memorySynchronize $world
    status=$?

    return $status
}

# Enable
doEnable() {
    world=$1

    serverWorldEnable $world
    status=$?

    return $status
}

# Disable
doDisable() {
    world=$1

    serverWorldDisable $world
    status=$?

    return $status
}

# Refresh
doRefresh() {
    world=$1

    serverWorldRefreshFiles $world
    status=$?

    return $status
}

# Start All
doStartAll() {
    serverWorldList
    status=$?

    if [ $status -eq 0 ]; then

        for((i=1;i<=${#WORLDSLIST[@]};i++))
        do
            consoleWrite "* ${WORLDSLIST[i]}..."
            doStart ${WORLDSLIST[i]}
        done

    else
        doPrintError
    fi
}

# Stop All
doStopAll() {
    serverWorldList
    status=$?

    if [ $status -eq 0 ]; then

        for((i=1;i<=${#WORLDSLIST[@]};i++))
        do
            consoleWrite "* ${WORLDSLIST[i]}..."
            doStop ${WORLDSLIST[i]}
        done

    else
        doPrintError
    fi
}

# Restart All
doRestartAll() {
    serverWorldList
    status=$?

    if [ $status -eq 0 ]; then

        for((i=1;i<=${#WORLDSLIST[@]};i++))
        do
            consoleWrite "* ${WORLDSLIST[i]}..."
            doRestart ${WORLDSLIST[i]}
        done

    else
        doPrintError
    fi
}

# Synchronize All
doSynchronizeAll() {
    serverWorldList
    status=$?

    if [ $status -eq 0 ]; then

        for((i=1;i<=${#WORLDSLIST[@]};i++))
        do
            consoleWrite "* ${WORLDSLIST[i]}..."
            doSynchronize ${WORLDSLIST[i]}
        done

    else
        doPrintError
    fi
}

# Backup All
doBackupAll() {
    serverWorldList
    status=$?

    if [ $status -eq 0 ]; then

        for((i=1;i<=${#WORLDSLIST[@]};i++))
        do
            consoleWrite "* ${WORLDSLIST[i]}..."
            doBackupWorld ${WORLDSLIST[i]}
        done

    else
        doPrintError
    fi
}

# Cleanup All
doCleanupAll() {
    serverWorldList
    status=$?

    if [ $status -eq 0 ]; then

        for((i=1;i<=${#WORLDSLIST[@]};i++))
        do
            consoleWrite "* ${WORLDSLIST[i]}..."
            doCleanup ${WORLDSLIST[i]}
        done

    else
        doPrintError
    fi
}

# Running All
doRunningAll() {
    serverWorldList
    status=$?

    if [ $status -eq 0 ]; then

        for((i=1;i<=${#WORLDSLIST[@]};i++))
        do
            if doStatus "${WORLDSLIST[i]}"; then
                consoleWrite "* ${WORLDSLIST[i]} : $M_WORLD_RUNNING..."
            else
                consoleWrite "* ${WORLDSLIST[i]} : $M_WORLD_NOT_RUNNING..."
            fi
        done

    else
        doPrintError
    fi
}

# List all server jar
serverJarList() {
    JARLIST=()
    TEMPO=/tmp/jars
    i=1

    echo '' > $TEMPO

    if [ $PAPER -eq 1 ]; then
        ls $DIRECTORY/$FILESFOLDER/paper-*.jar | sort -t '-' -nk2 | while read f; do echo $f >> $TEMPO; done;
    fi

    if [ $FABRIC -eq 1 ]; then
        ls $DIRECTORY/$FILESFOLDER/fabric-*.jar | sort -t '-' -nk2 | while read f; do echo $f >> $TEMPO; done;
    fi

    for jar in `cat $TEMPO`
    do
        jar="${jar%/}"
        jar=$( basename $jar )

        if [[ ! -z "$jar" && "$jar" != "*" ]]; then
            JARLIST[i++]=$jar
        fi
    done

    rm -f $TEMPO

    if [ ${#JARLIST[@]} -gt 0 ]; then
        return 0 # 0 = true
    else
        logWrite "$M_NO_JAR_FOUND"
        addError "$M_NO_JAR_FOUND"

        return 1 # 1 = false
    fi
}

# List all server jar
serverJarListDisplay() {
    for ((i=1; i<=${#JARLIST[@]}; i++))
    do
        consoleWrite "* ${JARLIST[i]}"
    done
}

# Cleanup uneeded server jar
serverJarCleanup() {
    elems=${#JARLIST[@]}
    let max=elems-2

    for ((i=1; i<=$max; i++))
    do
        if [ -f $DIRECTORY/$FILESFOLDER/${JARLIST[i]} ]; then
            rm -f $DIRECTORY/$FILESFOLDER/${JARLIST[i]}
        fi
    done
}

# Cleanup all after server upgrade
serverCleanupAllAfterUpgrade() {
    serverWorldList
    status=$?

    if [ $status -eq 0 ]; then

        for ((i=1;i<=${#WORLDSLIST[@]};i++))
        do
            consoleWrite "* ${WORLDSLIST[i]}..."
            serverWorldCleanupAfterUpdate ${WORLDSLIST[i]}
        done

    else
        doPrintError
    fi
}

# Check for server health
serverCheckHealth() {
    lockExist
    isLockExist=$?

    serverWorldListAll
    status=$?

    if [ $status -eq 0 ]; then

        badHealth=0
        worldsCount=0
        worldsExists=0
        worldsPids=0
        worldsRunning=0

        for ((i=1; i<=${#WORLDSLIST[@]}; i++))
        do
            worldsCount=$((worldsCount+1))

            world=${WORLDSLIST[i]}

            serverWorldExist $world
            status=$?

            if [ $status -eq 0 ]; then

                worldsExists=$((worldsExists+1))

                pidWorldExist $world
                status=$?

                if [ $status -eq 0 ]; then

                    worldsPids=$((worldsPids+1))

                    isWorldRunning $world
                    status=$?

                    if [ $status -eq 0 ]; then
                        worldsRunning=$((worldsRunning+1))
                    fi

                fi

            fi

        done

        if [ $worldsExists -ne $worldsCount ]; then

            logWrite "$M_SOME_WORLDS_INCONSTANCE"
            addError "$M_SOME_WORLDS_INCONSTANCE"
            consoleWrite "$M_SOME_WORLDS_INCONSTANCE"

            badHealth=1
        fi

        if [ $worldsRunning -lt $worldsPids ]; then

            logWrite "$M_SOME_WORLDS_CRASHED"
            addError "$M_SOME_WORLDS_CRASHED"
            consoleWrite "$M_SOME_WORLDS_CRASHED"

            badHealth=1
        fi

        if [ $badHealth -eq 1 ]; then
            return 1 # 1 = false
        fi

        return 0 # 0 = true
    else

        logWrite "$M_NO_WORLD_FOUND"
        addError "$M_NO_WORLD_FOUND"
        consoleWrite "$M_NO_WORLD_FOUND"

        return 0 # 0 = true
    fi
}
