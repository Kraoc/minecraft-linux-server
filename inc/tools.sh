#!/bin/bash
# LATEST: 2021-12-16
#
# APPLICATIONS
#

BASH=$(which bash)
TAR=$(which tar)
JAVA=$(which java)
PERL=$(which perl)
WGET=$(which wget)
ZIP=$(which pigz)
PYTHON=$(which python)
SCREEN=$(which screen)
PS=$(which ps)

#
# TOOLS
#

JAVAOPTS=""

# Run as user
execAsUser() {
    USER=$(whoami)
    if [ $USER == $USERNAME ] ; then
        bash -c "$1"
    else
        su - $USERNAME -c "$1"
    fi
}

# Adjust system ressources
doSystemAdjust() {
    JAVAOPTS="\
-XX:+EnableJVMCIProduct \
-XX:+EnableJVMCI \
-XX:+UseJVMCICompiler \
-XX:+EagerJVMCI \
-XX:+UnlockExperimentalVMOptions \
-XX:+UnlockDiagnosticVMOptions \
-XX:+UseNUMA \
-XX:+UseG1GC \
-XX:+DisableExplicitGC \
-XX:+ParallelRefProcEnabled \
-XX:+AlwaysPreTouch \
-XX:+PerfDisableSharedMem \
-XX:+OptimizeStringConcat \
-XX:+UseStringDeduplication \
-XX:+UseFastUnorderedTimeStamps \
-XX:+UseLoopPredicate \
-XX:+RangeCheckElimination \
-XX:+EliminateLocks \
-XX:+DoEscapeAnalysis \
-XX:+UseCodeCacheFlushing \
-XX:+UseInlineCaches \
-XX:+UseFastJNIAccessors \
-XX:+UseCompressedOops \
-XX:+OmitStackTraceInFastThrow \
-XX:+TrustFinalNonStaticFields \
-XX:+RewriteBytecodes \
-XX:+RewriteFrequentPairs \
-XX:-DontCompileHugeMethods \
-XX:+UseCMoveUnconditionally \
-XX:+UseFPUForSpilling \
-XX:+UseVectorCmov \
-XX:+UseXMMForArrayCopy \
-XX:+UseThreadPriorities \
-XX:ThreadPriorityPolicy=1 \
-XX:MaxGCPauseMillis=200 \
-XX:G1NewSizePercent=30 \
-XX:G1MaxNewSizePercent=40 \
-XX:G1HeapRegionSize=8M \
-XX:G1ReservePercent=20 \
-XX:G1HeapWastePercent=5 \
-XX:G1MixedGCCountTarget=4 \
-XX:G1MixedGCLiveThresholdPercent=90 \
-XX:G1RSetUpdatingPauseTimePercent=5 \
-XX:InitiatingHeapOccupancyPercent=15 \
-XX:SurvivorRatio=32 \
-XX:MaxTenuringThreshold=1 \
-Daikars.new.flags=true \
-XX:AllocatePrefetchStyle=1 \
-Dfile.encoding=UTF-8 \
-Djdk.nio.maxCachedBufferSize=262144 \
-Dusing.aikars.flags=https://mcflags.emc.gs \
-Dgraal.SpeculativeGuardMovement=true \
-Dgraal.CompilerConfiguration=community \
--add-modules jdk.incubator.vector \
-Dlog4j2.formatMsgNoLookups=true "

    if [ $RAM -ge 8192 ]; then
        if [ $ARCH -eq 64 ]; then
            MEMORYMAX=6144
            MEMORYMIN=$MEMORYMAX
        else
            MEMORYMAX=2300
            MEMORYMIN=$MEMORYMAX
        fi

    elif [ $RAM -ge 4096 ]; then
        if [ $ARCH -eq 64 ]; then
            MEMORYMAX=4096
            MEMORYMIN=$MEMORYMAX
        else
            MEMORYMAX=2300
            MEMORYMIN=$MEMORYMAX
        fi

    elif [ $RAM -ge 2300 ]; then
        MEMORYMAX=2300
        MEMORYMIN=$MEMORYMAX

    elif [ $RAM -ge 1024 ]; then
        MEMORYMAX=1024
        MEMORYMIN=$MEMORYMAX

    else
        if [ $RAM -ge 512 ]; then
            MEMORYMAX=512
            MEMORYMIN=$((MEMORYMAX / 2 ))
        else
            MEMORYMAX=$(( MEMORYMAX*33/100 ))
            MEMORYMIN=$((MEMORYMAX / 2 ))
        fi
    fi

    if [ $MEMORYCAP -ge 1 ]; then
        MEMORYMAX=$MEMORYCAP
        MEMORYMIN=$MEMORYMAX
    fi

    NUMBERCPU=$CPU
    JAVAOPTS="$JAVAOPTS-XX:ParallelGCThreads=${NUMBERCPU}"
}

# Adjust owner & rights
doAdjustOwnerRights() {
    chown -R $USERNAME:$USERNAME $DIRECTORY
    chown -R $USERNAME:$USERNAME $DIRECTORY/
    chown -R $USERNAME:$USERNAME $DIRECTORY/.
    chown root:root $DIRECTORY/files/minecraft-init-d
}

# Send a notification
doNotify() {
    message=$1
    
    m1=$message
    m1=${m1/T_WORLD/$world}
    m2=${m1/T_ERROR/$ERROR}
    m3=${m2/T_LOGS/$LOGS}
    m4=${m3/T_PID/$PID}
    m5=${m4/T_COMMAND/$command}
    message=$m5

    if [ -f $NOTIFY ]; then
        $BASH $NOTIFY "$M_TITLE" "$message"
    fi
}

# Cleanup textual backup files
doCleanupBak() {
    rm -rf $DIRECTORY/*.bak
}

# Print error message
doPrintError() {
    if [ "$ERROR" != "" ]; then
        consoleWrite "$M_FAILURE"
    else
        consoleWrite "$M_FAILURE_CAT"
    fi
}

# Print the call stack
printStack() {
    consoleWrite ""
    consoleWrite "--------------------------------------------------"
    consoleWrite "$M_CALL_STACK"

    local frame=0
    while caller $frame; do
        ((frame++));
    done

    consoleWrite "--------------------------------------------------"
    consoleWrite ""
}

# Add error message
addError() {
    error=$1

    if [ "$ERROR" = "" ]; then
        ERROR="$error"

        if [ $DEBUG -eq 1 ]; then
            printStack
        fi
    fi
}

# Add world error message
addErrorWorld() {
    world=$1
    error=$2

    addError "$world $error"
}

# Clear & free memory
doFreeMemory() {
    sync
}

# Send a notification
doUpload() {
    file=$1

    if [ -f $UPLOAD ]; then
        $BASH $UPLOAD $file
    fi
}
