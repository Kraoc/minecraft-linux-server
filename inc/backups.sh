#!/bin/bash
# LATEST: 2021-12-21
#
# BACKUPS
#

BACKUPBASE=$DIRECTORY/backups


# Backup server
doBackupServer() {
    doNotify "$M_SERVER_BACKUP_STARTING"
    NOW=`date "+%Y%m%d_%H%M%S"`
    FILE=$BACKUPBASE/minecraft_server_$NOW.tar

    # create folder if needed
    if [ ! -d $BACKUPBASE ]; then
        mkdir -p $BACKUPBASE
        chown -R $USERNAME:$USERNAME $BACKUPBASE/.
        chmod 0777 $BACKUPBASE
    fi

    # communicate
    logWrite "$M_SERVER_BACKUP_STARTING"

    # create archive
    logWrite "$M_CREATE_ARCHIVE"
    execAsUser "$TAR -P -C \"$DIRECTORY\" -cf \"$FILE\" \
                    --exclude='_work' \
                    --exclude='build' \
                    --exclude='backups' \
                    --exclude='memory' \
                    --exclude='sync' \
                    --exclude='plugins/disabled' \
                    --exclude='mods/disabled' \
                    --exclude='run' \
                    --exclude='worlds' \
                    --exclude='*.bak' \
                    --exclude='notify' \
                    --exclude='postserverbackup' \
                    --exclude='postworldbackup' \
                    --exclude='upload' \
                    --exclude='minecraft.disable' \
                    --exclude='worlds/*' \
                    --exclude='logs/*.gz' \
                    --exclude='server.log' \
                    --exclude='files/*.sh' \
                    --exclude='files/*.jar' \
                    --exclude='files/*.png' \
                    --exclude='files/*.yml' \
                    --exclude='files/*.txt' \
                    --exclude='files/server.properties' \
                    --exclude='files/*.py' \
                    --exclude='files/minecraft-init-d' \
                    --exclude='files/.fabric-installer' \
                    --exclude='files/libraries' \
                    --exclude='files/fabric-server-launcher.properties' \
                    $DIRECTORY/. " &> /dev/null
    chown -R $USERNAME:$USERNAME $FILE
    sync

    # compress archive
    logWrite "$M_COMPRESS_ARCHIVE"
    execAsUser "$ZIP --best \"$FILE\""
    chown -R $USERNAME:$USERNAME $FILE.gz
    chmod 0666 $FILE.gz
    sync

    # post operations
    if [ -f $DIRECTORY/postserverbackup ]; then
        bash $DIRECTORY/postserverbackup $FILE.gz
    fi

    logWrite "$M_SERVER_BACKUP_FINISHED"
    doNotify "$M_SERVER_BACKUP_FINISHED"

    return 0 # 0 = true
}

# Backup world
doBackupWorld() {
    world=$1
    doNotify "$M_WORLD_BACKUP_STARTING"
    NOW=`date "+%Y%m%d_%H%M%S"`
    FILE=$BACKUPBASE/minecraft_worlds_${world}_$NOW.tar

    # create folder if needed
    if [ ! -d $BACKUPBASE ]; then
        mkdir -p $BACKUPBASE
        chown -R $USERNAME:$USERNAME $BACKUPBASE/.
        chmod 0777 $BACKUPBASE
    fi

    # communicate
    logWorldWrite $world "$M_BACKUP_STARTING"

    # create archive
    doSaveOff $world
    execAsUser "$TAR -P -C \"$WORLDSBASE/$world\" -cf \"$FILE\" \
                    --exclude='cache' \
                    --exclude='logs' \
                    --exclude='plugins' \
                    --exclude='mods' \
                    --exclude='bluemap' \
                    --exclude='*.jar' \
                    --exclude='*.sh' \
                    --exclude='*.py' \
                    --exclude='*.png' \
                    --exclude='.fabric' \
                    --exclude='.mixin.out' \
                    --exclude='crash-reports' \
                    --exclude='libraries' \
                    --exclude='versions' \
                    world/. \
                    world_nether/. \
                    world_the_end/. \
                    banned-ips.json \
                    banned-players.json \
                    ops.json \
                    permissions.yml \
                    server.properties \
                    usercache.json \
                    whitelist.json" &> /dev/null
    chown -R $USERNAME:$USERNAME $FILE
    sync
    doSaveOn $world

    # compress archive
    execAsUser "$ZIP --best \"$FILE\""
    chown -R $USERNAME:$USERNAME $FILE.gz
    chmod 0666 $FILE.gz
    sync

    # post operations
    if [ -f $DIRECTORY/postworldbackup ]; then
        bash $DIRECTORY/postworldbackup $FILE.gz
    fi

    logWorldWrite $world "$M_BACKUP_FINISHED"
    doNotify "$M_WORLD_BACKUP_FINISHED"

    return 0 # 0 = true
}
