#!/bin/bash
# LATEST: 2021-12-14
#
# PAPER
#

# compute paper server jar download url
doBuildRemoteUrl() {
    version=$1
    PAPERVERSION=1.17.1

    REMOTEURL=https://papermc.io/api/v2/projects/paper/versions/$PAPERVERSION/builds/$version/downloads/paper-$PAPERVERSION-$version.jar
}
