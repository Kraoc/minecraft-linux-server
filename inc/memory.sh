#!/bin/bash
# LATEST: 2021-12-14
#
# MEMORY
#

#
MEMLOCKIDBASE=$LOCKIDBASE

#
# LOCKS
#

# Check memory world lock file
memoryWorldLockExist() {
    if [ ! -d $MEMLOCKIDBASE ]; then
        mkdir -p $MEMLOCKIDBASE
        chown -R $USERNAME:$USERNAME $MEMLOCKIDBASE
    fi

    world=$1
    lockid=$MEMLOCKIDBASE/memory.$world.lock

    if [ -f $lockid ]; then
        return 0 # 0 = true
    else
        return 1 # 1 = false
    fi
}

# Set memory world lock
memoryWorldLockSet() {
    world=$1
    lockid=$MEMLOCKIDBASE/memory.$world.lock

    logWorldWrite $world "$M_MEMORY_LOCK_CREATION"

    memoryWorldLockExist $world
    status=$?

    if [ $status -eq 1 ]; then
        touch $lockid
        chown -R $USERNAME:$USERNAME $lockid

        logWorldWrite $world "$M_MEMORY_LOCK_SET"

        return 0 # 0 = true
    else
        logWorldWrite $world "$M_MEMORY_NOT_RUNNING"
        addErrorWorld $world "$M_MEMORY_NOT_RUNNING_LOCK_SET"

        return 1 # 1 = false
    fi
}

# Unset memory world lock
memoryWorldLockUnset() {
    world=$1
    lockid=$MEMLOCKIDBASE/memory.$world.lock

    logWorldWrite $world "$M_MEMORY_LOCK_REMOVING"

    memoryWorldLockExist $world
    status=$?

    if [ $status -eq 0 ]; then
        rm -f $lockid

        logWorldWrite $world "$M_MEMORY_LOCK_UNSET"

        return 0 # 0 = true
    else
        logWorldWrite $world "$M_MEMORY_NOT_RUNNING"
        addErrorWorld $world "$M_MEMORY_NOT_RUNNING_LOCK_UNSET"

        return 1 # 1 = false
    fi
}


#
# FOLDERS
#

# Set memory world folder
memoryWorldFolderSet() {
    world=$1

    memoryWorldLockExist $world
    status=$?

    if [ $status -eq 1 ]; then
        logWorldWrite $world "$M_MEMORY_FOLDER_CREATION"

        folder=$MEMORYBASE/$world
        mkdir -m 755 -p $folder
        chown -R $USERNAME:$USERNAME $MEMORYBASE
        chown -R $USERNAME:$USERNAME $folder

        logWorldWrite $world "$M_MEMORY_FOLDER_SET"

        return 0 # 0 = true
    else
        logWorldWrite $world "$M_MEMORY_NOT_RUNNING"
        addErrorWorld $world "$M_MEMORY_NOT_RUNNING_FOLDER_SET"

        return 1 # 1 = false
    fi
}

# Unset memory world folder
memoryWorldFolderUnset() {
    world=$1

    memoryWorldLockExist $world
    status=$?

    if [ $status -eq 0 ]; then
        logWorldWrite $world "$M_MEMORY_FOLDER_REMOVING"

        rmdir $MEMORYBASE/$world

        logWorldWrite $world "$M_MEMORY_FOLDER_UNSET"

        return 0 # 0 = true
    else
        logWorldWrite $world "$M_MEMORY_NOT_RUNNING"
        addErrorWorld $world "$M_MEMORY_NOT_RUNNING_FOLDER_UNSET"

        return 1 # 1 = false
    fi
}


#
# OPERATIONS
#

# Create memory world folder
memoryWorldFolderCreate() {
    world=$1

    memoryWorldFolderSet $world
    status=$?

    if [ $status -eq 0 ]; then

        memoryWorldLockSet $world
        status=$?

        if [ $status -eq 0 ]; then
            logWorldWrite $world "$M_MEMORY_FOLDER_CREATED"

            return 0 # 0 = true
        else
            memoryWorldLockUnset $world
            memoryWorldFolderUnset $world

            logWorldWrite $world "$M_MEMORY_FOLDER_CREATION_FAILURE"
            addErrorWorld $world "$M_MEMORY_FOLDER_CREATION_FAILURE"

            return 1 # 1 = false
        fi
    else
        return 1 # 1 = false
    fi
}

# Delete memory world folder
memoryWorldFolterDelete() {
    world=$1

    memoryWorldLockExist $world
    status=$?

    if [ $status -eq 0 ]; then

        memoryWorldLockUnset $world
        status=$?

        if [ $status -eq 0 ]; then
            memoryWorldFolderUnset $world

            logWorldWrite $world "$M_MEMORY_FOLDER_DELETED"

            return 0 # 0 = true
        else
            logWorldWrite $world "$M_MEMORY_FOLDER_DELETION_FAILURE"
            addErrorWorld $world "$M_MEMORY_FOLDER_DELETION_FAILURE"

            return 1 # 1 = false
       fi
    fi
}

# Copy datas from disk to memory
memoryWorldDisk2Memory() {
    world=$1
    memory=$MEMORYBASE/$world
    disk=$WORLDSBASE/$world

    memoryWorldLockExist $world
    status=$?

    if [ $status -eq 0 ]; then

        # create world folders
        mkdir -p $memory/logs/
        mkdir -p $memory/plugins/
        mkdir -p $memory/mods/
        mkdir -p $memory/world/
        mkdir -p $memory/world_nether/
        mkdir -p $memory/world_the_end/

        # copy world disk folders to memory
        cp -R $disk/logs $memory/
        cp -R $disk/plugins $memory/
        cp -R $disk/world $memory/
        cp -R $disk/world_nether $memory/
        cp -R $disk/world_the_end $memory/

        # delete disk world folders
        rm -rf $disk/logs
        rm -rf $disk/plugins
        rm -rf $disk/mods
        rm -rf $disk/world
        rm -rf $disk/world_nether
        rm -rf $disk/world_the_end

        # simlink world folders
        cd $disk
        ln -s $(realpath --relative-to=$disk $memory)/logs $disk/logs
        ln -s $(realpath --relative-to=$disk $memory)/plugins $disk/plugins
        ln -s $(realpath --relative-to=$disk $memory)/mods $disk/mods
        ln -s $(realpath --relative-to=$disk $memory)/world $disk/world
        ln -s $(realpath --relative-to=$disk $memory)/world_nether $disk/world_nether
        ln -s $(realpath --relative-to=$disk $memory)/world_the_end $disk/world_the_end
        cd $DIRECTORY

        # setup permissions
        chown -R $USERNAME:$USERNAME $memory
        chown -R $USERNAME:$USERNAME $disk

        # ok
        logWorldWrite $world "$M_MEMORY_DISK_TO_MEMORY_COPIED"

        return 0 # 0 = true
    else
        logWorldWrite $world "$M_MEMORY_NOT_RUNNING"
        addErrorWorld $world "$M_MEMORY_NOT_RUNNING_DISK_TO_MEMORY"

        return 1 # 1 = false
    fi
}

# Copy datas from memory to disk
memoryWorldMemory2Disk() {
    world=$1
    memory=$MEMORYBASE/$world
    disk=$WORLDSBASE/$world

    memoryWorldLockExist $world
    status=$?

    if [ $status -eq 0 ]; then

        # remove world folders symlinks
        rm -f $disk/logs
        rm -f $disk/plugins
        rm -f $disk/mods
        rm -f $disk/world
        rm -f $disk/world_nether
        rm -f $disk/world_the_end

        # recreate disk world folders
        mkdir -p $disk/logs
        mkdir -p $disk/plugins
        mkdir -p $disk/mods
        mkdir -p $disk/world
        mkdir -p $disk/world_nether
        mkdir -p $disk/world_the_end

        # copy world memory folders to disk
        cp -R $memory/logs $disk
        cp -R $memory/plugins $disk
        cp -R $memory/mods $disk
        cp -R $memory/world $disk
        cp -R $memory/world_nether $disk
        cp -R $memory/world_the_end $disk

        # remove memory folders
        rm -rf $memory

        # setup permissions
        chown -R $USERNAME:$USERNAME $disk

        # ok
        logWorldWrite $world "$M_MEMORY_MEMORY_TO_DISK_COPIED"

        return 0 # 0 = true
    else
        logWorldWrite $world "$M_MEMORY_NOT_RUNNING"
        addErrorWorld $world "$M_MEMORY_NOT_RUNNING_MEMORY_TO_DISK"

        return 1 # 1 = false
    fi
}

# Synchronize memory world folders to disk
memoryWorldSynchronizeMemory() {
    world=$1
    memory=$MEMORYBASE/$world
    disk=$SYNCBASE/$world

    memoryWorldLockExist $world
    status=$?

    if [ $status -eq 0 ]; then

        # memory world folder creation on demand
        if [ ! -d $disk ]
        then
            mkdir -m 755 -p $disk
            chown $USERNAME:$USERNAME $SYNCBASE
            chown -R $USERNAME:$USERNAME $disk
        fi
        doSay $world "$M_WORLD_SYNCHRONIZATION"

        # synchronize
        doSaveOff $world
        rsync -r -t -v -L -q $memory/. $disk/.
        doSaveOn $world

        # setup permissions
        chown -R $USERNAME:$USERNAME $disk

        # ok
        logWorldWrite $world "$M_MEMORY_SYNCHRONIZED"

        return 0 # 0 = true
    else
        logWorldWrite $world "$M_MEMORY_NOT_RUNNING"
        addErrorWorld $world "$M_MEMORY_NOT_RUNNING_SYNCHRONIZE"

        return 1 # 1 = false
    fi
}

# Create memory world
memoryWorldCreate() {
    world=$1

    memoryWorldLockExist $world
    status=$?

    if [ $status -eq 1 ]; then

        # create world folder
        memoryWorldFolderCreate $world
        status=$?

        if [ $status -eq 0 ]; then

            # copy disk to memory
            memoryWorldDisk2Memory $world
            status=$?

            if [ $status -eq 0 ]; then

                # synchronize
                memoryWorldSynchronizeMemory $world
                status=$?

                if [ $status -eq 0 ]; then

                    # pid creation
                    pidMemorySet $world
                    status=$?

                    if [ $status -eq 0 ]; then

                        # lock creation
                        memoryWorldLockSet $world
                        logWorldWrite $world "$M_MEMORY_CREATED"
                        return 0 # 0 = true
                    else
                        pidMemoryUnset $world
                        memoryWorldMemory2Disk $world
                        memoryWorldFolterDelete $world

                        logWorldWrite $world "$M_MEMORY_PID_CREATION_FAILURE"
                        addErrorWorld $world "$M_MEMORY_PID_CREATION_FAILURE"

                        return 1 # 1 = false
                    fi
                else
                    memoryWorldMemory2Disk $world
                    memoryWorldFolterDelete $world

                    logWorldWrite $world "$M_MEMORY_SYNCHRONIZATION_FAILURE"
                    addErrorWorld $world "$M_MEMORY_SYNCHRONIZATION_FAILURE"

                    return 1 # 1 = false
                fi
            else
                memoryWorldFolterDelete $world

                logWorldWrite $world "$M_MEMORY_DISK_TO_MEMORY_FAILURE"
                addErrorWorld $world "$M_MEMORY_DISK_TO_MEMORY_FAILURE"

                return 1 # 1 = false
            fi
        else
            logWorldWrite $world "$M_MEMORY_CREATION_FAILURE"
            addErrorWorld $world "$M_MEMORY_CREATION_FAILURE"

            return 1 # 1 = false
        fi
    else
        logWorldWrite $world "$M_MEMORY_EXIST"
        addError "$M_MEMORY_EXIST"

        return 1 # 1 = false
    fi

}

# Delete memory world
memoryWorldDelete() {
    world=$1

    memoryWorldLockExist $world
    status=$?

    if [ $status -eq 0 ]; then

        # synchronize memory
        memoryWorldSynchronizeMemory $world
        status=$?

        if [ $status -eq 0 ]; then

            # copy memory to disk
            memoryWorldMemory2Disk $world
            status=$?

            if [ $status -eq 0 ]; then

                # remove memory folder
                memoryWorldFolterDelete $world
                status=$?

                if [ $status -eq 0 ]; then

                    memoryWorldLockUnset $world
                    pidMemoryUnset $world
                    logWorldWrite $world "$M_MEMORY_DELETED"

                    return 0 # 0 = true
                else
                    logWorldWrite $world "$M_MEMORY_FOLDER_REMOVE_FAILURE"
                    addErrorWorld $world "$M_MEMORY_FOLDER_REMOVE_FAILURE"

                    return 1 # 1 = false
                fi
            else
                logWorldWrite $world "$M_MEMORY_MEMORY_TO_DISK_FAILURE"
                addErrorWorld $world "$M_MEMORY_MEMORY_TO_DISK_FAILURE"

                return 1 # 1 = false
            fi
        else
            logWorldWrite $world "$M_MEMORY_SYNCHRONIZATION_FAILURE"
            addErrorWorld $world "$M_MEMORY_SYNCHRONIZATION_FAILURE"

            return 1 # 1 = false
        fi
    else
        logWorldWrite $world "$M_MEMORY_NOT_EXIST"
        addErrorWorld $world "$M_MEMORY_NOT_EXIST"

        return 1 # 1 = false
    fi
}

# Repair world from memory
memoryWorldRepair() {
    world=$1

    memoryWorldLockExist $world
    status=$?

    if [ $status -eq 0 ]; then

        folder=$MEMORYBASE/$world

        if [[ -d $folder/logs && -d $folder/plugins && -d $folder/mods && -d $folder/world && -d $folder/world_nether && -d $folder/world_the_end ]]; then

            memoryWorldMemory2Disk $world
            status=$?

            if [ $status -eq 0 ]; then
                memoryWorldLockUnset $world
                return 0 # 0 = true
            else
                return 1 # 1 = false
            fi
        else
            logWorldWrite $world "$M_MEMORY_FOLDER_NOT_COMPLETE"
            addErrorWorld $world "$M_MEMORY_FOLDER_NOT_COMPLETE"

            return 1 # 1 = false
        fi
    else
        return 1 # 1 = false
    fi
}

# Repair world from sync
syncWorldRepair() {
    world=$1

    memoryWorldLockExist $world
    status=$?

    if [ $status -eq 0 ]; then

        memory=$MEMORYBASE/$world
        disk=$WORLDSBASE/$world
        folder=$SYNCBASE/$world

        if [[ -d $folder/logs && -d $folder/plugins && -d $folder/mods && -d $folder/world && -d $folder/world_nether && -d $folder/world_the_end ]]; then

            rm -f $disk/logs
            rm -f $disk/plugins
            rm -f $disk/mods
            rm -f $disk/world
            rm -f $disk/world_nether
            rm -f $disk/world_the_end

            # recreate disk world folders
            mkdir -p $disk/logs
            mkdir -p $disk/plugins
            mkdir -p $disk/mods
            mkdir -p $disk/world
            mkdir -p $disk/world_nether
            mkdir -p $disk/world_the_end

            # copy world memory folders to disk
            cp -R $folder/logs $disk
            cp -R $folder/plugins $disk
            cp -R $folder/mods $disk
            cp -R $folder/world $disk
            cp -R $folder/world_nether $disk
            cp -R $folder/world_the_end $disk

            # setup permissions
            chown -R $USERNAME:$USERNAME $folder

            memoryWorldLockUnset $world

            return 0 # 0 = true
        else
            logWorldWrite $world "$M_SYNC_FOLDER_NOT_COMPLETE"
            addErrorWorld $world "$M_SYNC_FOLDER_NOT_COMPLETE"

            return 1 # 1 = false
        fi
    else
        return 1 # 1 = false
    fi
}

#
# META OPERATIONS
#

# Create memory world
memoryCreate() {
    if [ $RAMDISK -eq 1 ]; then
        world=$1

        memoryWorldCreate $world
        status=$?

        return $status
    else
        return 0 # 0 = true
    fi
}

# Delete memory world
memoryDelete() {
    if [ $RAMDISK -eq 1 ]; then
        world=$1

        memoryWorldDelete $world
        status=$?

        return $status
    else
        return 0 # 0 = true
    fi
}

# Synchronize memory world
memorySynchronize() {
    if [ $RAMDISK -eq 1 ]; then
        world=$1

        memoryWorldSynchronizeMemory $world
        status=$?

        return $status
    else
        return 0 # 0 = true
    fi
}

# Repair world from memory
memoryRepair() {
    if [ $RAMDISK -eq 1 ]; then
        world=$1

        memoryWorldRepair $world
        status=$?

        return $status
    else
        return 0 # 0 = true
    fi
}

# Repair world from sync
syncRepair() {
    if [ $RAMDISK -eq 1 ]; then
        world=$1

        syncWorldRepair $world
        status=$?

        return $status
    else
        return 0 # 0 = true
    fi
}
