#!/bin/bash
# LATEST: 2021-12-15
#
# COMMUNICATE
#

#
# Basic communication command
#

# Send a server command
doCommand() {
    world=$1

    isWorldRunning $world
    status=$?

    if [ $status -eq 0 ]; then

        command=$2
        SNW=$SCREENNAME$world
        
        execAsUser "$SCREEN -p 0 -S $SNW -X eval 'stuff \"$command\"\015'"
        logWorldWrite $world "$M_COMMAND_SENT"

        sleep 1
    else
        logWorldWrite $world "$M_SERVER_NOT_RUNNING"
    fi
}

#
# Interpreted commands
#

# Issue 'say' command to server
doSay() {
    world=$1    
    message=$2
    
    m1=$message
    m1=${m1/T_WORLD/$world}
    m2=${m1/T_ERROR/$ERROR}
    m3=${m2/T_LOGS/$LOGS}
    m4=${m3/T_PID/$PID}
    m5=${m4/T_COMMAND/$command}
    message=$m5

    doCommand $world "say $message"
}

# Server command: save-off
doSave_Off() {
    world=$1
    doCommand $world "save-off"
}

# Server command: save-on
doSave_On() {
    world=$1
    doCommand $world "save-on"
}

# Server command: save-all
doSave_All() {
    world=$1
    doCommand $world "save-all"
}

# Server command: op
doOp() {
    world=$1
    user=$2
    doCommand $world "op $user"
}

# Server command: deop
doDeop() {
    world=$1
    user=$2
    doCommand $world "deop $user"
}

# Server command: kick
doKick() {
    world=$1
    user=$2
    doCommand $world "kick $user"
}

# Server command: ban
doBan() {
    world=$1
    user=$2
    doCommand $world "ban $user"
}

# Server command: pardon
doUnban() {
    world=$1
    user=$2
    doCommand $world "pardon $user"
}

#
# Combined commands
#

# Prepare server backup
doSaveOff() {
    world=$1

    lockWorldExist $world
    status=$?

    if [ $status -eq 0 ]; then

        logWorldWrite $world "$M_SUSPENDING_SAVE"
        doSay $world "$M_SERVER_BACKUP_STARTING"
        doSave_Off $world
        doSave_All $world

        sync
    else
        logWorldWrite $world "$M_SERVER_NOT_RUNNING"
    fi
}

# End server backup
doSaveOn() {
    world=$1

    lockWorldExist $world
    status=$?

    if [ $status -eq 0 ]; then

        logWorldWrite $world "$M_ENABLING_SAVE"
        doSave_On $world
        sync

        doSay $world "$M_SERVER_BACKUP_ENDED"
    else
        logWorldWrite $world "$M_RESUMING_SAVE"
    fi
}

