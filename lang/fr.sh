#!/bin/bash
# LATEST: 2021-12-16
#
# fr_fr
#

M_TITLE="$GAMETITLE v$VERSION"

M_UNABLE_TO_SET_GLOBAL_LOCK="Impossible de définir un verrou global"
M_MAYBE_ALREADY_RUNNING="Peut-être déjà en cours d'exécution"
M_NO_SERVER_FOUND="Aucun serveur trouvé !"
M_PLEASE_DO_DOWNLOAD="Veuillez télécharger la 'version'".
M_DOWNLOADING_SERVER="Téléchargement du serveur..."
M_DOWNLOADING_INSTALLER="Téléchargement de l'installeur..."
M_DONE="Terminé"
M_INVALID_ARGUMENTS="Arguments non valides".
M_CREATING_WORLD="Création du monde (T_WORLD)..."
M_REFRESHING_WORLD_PLUGINS="Rafraîchi les modules d'extension du monde (T_WORLD)..."
M_STARTING_WORLD="Démarrage du monde (T_WORLD)..."
M_STOPPING_WORLD="Arrêt du monde (T_WORLD)..."
M_RESTARTING_WORLD="Redémarrage du monde (T_WORLD)..."
M_SYCHNRONIZING_WORLD="Sychroniser le monde (T_WORLD)..."
M_BACKING_UP_SERVER="Sauvegarde du serveur..."
M_BACKING_UP_WORLD="Sauvegarde du monde (T_WORLD)..."
M_WORLD_RUNNING="Monde en cours d'exécution"
M_WORLD_NOT_RUNNING="Monde pas en cours d'exécution"
M_WORLD_REPAIRED="Monde réparé"
M_UNABLE_TO_REPAIR_WORLD="Impossible de réparer le monde"
M_WORLD_CLEANED="Monde nettoyé"
M_UNABLE_TO_CLEAN_WORLD="Impossible de nettoyer le monde"
M_ENABLING_WORLD="Activation du monde (T_WORLD)..."
M_DISABLING_WORLD="Monde désactivé (T_WORLD)..."
M_REFRESHING_WORLD="Monde rafraîchi (T_WORLD)..."
M_MUST_SPECIFY_SERVER_COMMAND="Vous devez spécifier la commande du serveur (essayez 'help' ?)"
M_ADDING_OPERATOR="Ajout d'un opérateur..."
M_REMOVING_OPERATOR="Retrait de l'opérateur..."
M_KICKING_PLAYER="Exclure un joueur..."
M_BAN_PLAYER="Interdire un joueur..."
M_UNBAN_PLAYER="Authoriser un joueur..."
M_LISTINGS_WORLDS="Lister les mondes..."
M_LISTINGS_SERVER_JAR="Liste des fichiers jar du serveur..."
M_STARTING_ALL_WORLDS="Démarrage de tous les mondes..."
M_STOPPING_ALL_WORLDS="Arrêt de tous les mondes..."
M_RESTARTING_ALL_WORLDS="Redémarrer tous les mondes..."
M_SYCHNRONIZING_ALL_WORLDS="Sychroniser tous les mondes..."
M_CLEANING_ALL_WORLDS="Nettoyer tous les mondes..."
M_BACKING_UP_ALL_WORLDS="Sauvegarde de tous les mondes..."
M_NO_OR_UNKNOWN_COMMAND="Pas de commande ou commande inconnue (essayez 'help' ?)"
M_FAILURE="Échec (T_ERROR)"
M_FAILURE_CAT="Échec (cat T_LOGS)"
M_CALL_STACK="CALL STACK"
M_INITIALIZING_FILES="Initialiser les fichiers"
M_FILES_INITIALIZED="Fichiers initialisés"
M_SERVER_DOWNLOADED="Serveur téléchargé"
M_SERVER_DOWNLOAD_FAILURE="Echec du téléchargement sur le serveur"
M_SERVER_VERSION_ALREADY_DOWNLOADED="Version du serveur déjà téléchargée"
M_SERVER_POPULATED="Serveur installé"
M_NO_SERVER_DOWNLOADED="Aucun serveur téléchargé"
M_SERVER_BACKUP_STARTING="La sauvegarde du serveur commence"
M_CREATE_ARCHIVE="Créer une archive"
M_COMPRESS_ARCHIVE="Compression de l'archive"
M_SERVER_BACKUP_FINISHED="Sauvegarde du serveur terminée"
M_FINISHED="Terminé"
M_STARTING=">>> Démarrage"
M_NO_JAR_FOUND="Aucun jar trouvé"
M_NO_WORLD_FOUND="Aucun monde trouvé"
M_PID_CREATED="Un PID (T_PID) a été créé"
M_PID_REMOVED="PID supprimé"
M_MEMORY_PID_CREATED="Création d'un PID de mémoire"
M_MEMORY_PID_REMOVED="PID de la mémoire supprimé"
M_BACKUP_STARTING="Démarrage de la sauvegarde"
M_BACKUP_FINISHED="Sauvegarde terminée"
M_COMMAND_SENT="Commande (T_COMMAND) envoyée"
M_SERVER_NOT_RUNNING="Le serveur ne fonctionne pas"
M_SUSPENDING_SAVE="Suspendre la sauvegarde"
M_ENABLING_SAVE="Activation de la sauvegarde"
M_RESUMING_SAVE="Reprise de la sauvegarde"
M_MEMORY_LOCK_CREATION="Création d'un verrou de mémoire"
M_MEMORY_LOCK_SET="Blocage de la mémoire"
M_MEMORY_NOT_RUNNING="Mémoire non en cours d'exécution"
M_MEMORY_LOCK_REMOVING="Déverrouillage de la mémoire"
M_MEMORY_LOCK_UNSET="Verrouillage de la mémoire non activé"
M_MEMORY_FOLDER_CREATION="Création d'un dossier mémoire"
M_MEMORY_FOLDER_SET="Dossier mémoire créé"
M_MEMORY_FOLDER_REMOVING="Suppression d'un dossier mémoire"
M_MEMORY_FOLDER_UNSET="Dossier mémoire désactivé"
M_MEMORY_FOLDER_CREATED="Dossier mémoire créé"
M_MEMORY_FOLDER_CREATION_FAILURE="Échec de la création d'un dossier mémoire"
M_MEMORY_FOLDER_DELETED="Dossier mémoire supprimé"
M_MEMORY_FOLDER_DELETION_FAILURE="Échec de la suppression d'un dossier mémoire"
M_MEMORY_DISK_TO_MEMORY_COPIED="Copie d'un disque mémoire vers la mémoire"
M_MEMORY_MEMORY_TO_DISK_COPIED="Copie de la mémoire sur le disque"
M_MEMORY_SYNCHRONIZED="Mémoire synchronisée"
M_MEMORY_CREATED="Mémoire créée"
M_MEMORY_PID_CREATION_FAILURE="Échec de la création du PID de la mémoire"
M_MEMORY_SYNCHRONIZATION_FAILURE="Échec de la synchronisation de la mémoire"
M_MEMORY_DISK_TO_MEMORY_FAILURE="Échec du transfert du disque vers la mémoire"
M_MEMORY_CREATION_FAILURE="Échec de la création de mémoire"
M_MEMORY_EXIST="La mémoire existe"
M_MEMORY_DELETED="Mémoire supprimée"
M_MEMORY_FOLDER_REMOVE_FAILURE="Échec de la suppression d'un dossier mémoire"
M_MEMORY_MEMORY_TO_DISK_FAILURE="Échec du transfert de la mémoire vers le disque"
M_MEMORY_NOT_EXIST="Mémoire inexistante"
M_MEMORY_FOLDER_NOT_COMPLETE="Dossier mémoire non complet"
M_SYNC_FOLDER_NOT_COMPLETE="Dossier de synchronisation incomplet"
M_FOUND="Trouvé"
M_NOT_FOUND="Pas trouvé"
M_REFRESH_PLUGINS="Rafraîchir les modules d'extension"
M_PLUGINS_REFRESHED="Les plugins sont rafraîchis"
M_DONT_EXIST="N'existe pas"
M_CREATING="Créer"
M_REFRESHING="Rafraîchir"
M_CREATE_FOLDER="Création de dossier"
M_LINK_FILES="Lier les fichiers"
M_COPY_FILES="Copier les fichiers"
M_CREATE_DIRECTORIES="Créer des répertoires"
M_PATCH_FILES="Mise à jour des fichiers"
M_SETUP_RIGHTS="Droits d'accès"
M_CREATED="Créé"
M_REFRESHED="Rafraîchi"
M_ALREADY_EXIST="Existe déjà"
M_STARTING="Démarrage"
M_RUNNING="En cours d'exécution"
M_PID_CREATION_FAILURE="Échec de la création d'un PID"
M_ALREADY_RUNNING="Déjà en cours d'exécution"
M_STOPPING="En cours d'exécution"
M_STOPPED="Arrêté"
M_STOPPING_FAILURE="Échec de l'arrêt"
M_NOT_RUNNING="Ne fonctionne pas"
M_CRASHED_NEED_TO_REPAIR="Crashé, doit être réparé !"
M_OK_WORLD_RECOVERED_FROM_MEMORY="Ok, le monde a été récupéré de la mémoire"
M_OK_WORLD_RECOVERED_FROM_SYNC="Ok, le monde a été récupéré de la synchronisation"
M_UNABLE_TO_REPAIR_FROM_MEMORY_OR_SYNC="Impossible de réparer à partir de la mémoire ou de la synchronisation"
M_OK_NO_NEED_TO_REPAIR="Ok, pas besoin de réparer"
M_IS_ALREADY_DISABLED="Est déjà désactivé"
M_IS_RUNNING="Est en cours d'exécution"
M_IS_ALREADY_ENABLED="Est déjà activé"
M_LOCK_SET_FAILURE="Échec de l'établissement d'un verrou"
M_LOCK_UNSET_FAILURE="Échec du déverrouillage"
M_PID_SET_FAILURE="Échec de l'activation du PID"
M_PID_UNSET_FAILURE="Échec de la réinitialisation du PID"
M_MEMORY_PID_SET_FAILURE="Échec de l'établissement du PID en mémoire"
M_MEMORY_PID_UNSET_FAILURE="Échec de la réinitialisation du pid de la mémoire"
M_MEMORY_NOT_RUNNING_LOCK_SET="La mémoire n'est pas en cours d'exécution (verrou fixé)"
M_MEMORY_NOT_RUNNING_LOCK_UNSET="La mémoire n'est pas en cours d'exécution (lock unset)"
M_MEMORY_NOT_RUNNING_FOLDER_SET="La mémoire ne fonctionne pas (dossier défini)"
M_MEMORY_NOT_RUNNING_FOLDER_UNSET="La mémoire ne fonctionne pas (dossier non défini)"
M_MEMORY_NOT_RUNNING_DISK_TO_MEMORY="La mémoire ne fonctionne pas (disque en mémoire)"
M_MEMORY_NOT_RUNNING_MEMORY_TO_DISK="La mémoire ne fonctionne pas (de la mémoire vers le disque)"
M_MEMORY_NOT_RUNNING_SYNCHRONIZE="La mémoire ne fonctionne pas (synchronisation)"
M_WORLD_BACKUP_STARTING="Démarrage de la sauvegarde du monde (T_WORLD)"
M_WORLD_BACKUP_FINISHED="La sauvegarde du monde (T_WORLD) est terminée"
M_WORLD_STARTING="T_WORLD en cours de démarrage"
M_WORLD_RUNNING="T_WORLD en cours d'exécution"
M_WORLD_STOPPING="T_WORLD en cours d'arrêt"
M_WORLD_STOPPED="T_WORLD arrêté"
M_WORLD_DISABLING="T_WORLD désactivé"
M_WORLD_ENABLING="T_WORLD activé"
M_SERVER_BACKUP_STARTING="Démarrage de la sauvegarde du serveur"
M_SERVER_BACKUP_ENDED="Fin de la sauvegarde du serveur"
M_SHUTTING_DOWN="Arrêt du serveur"
M_CLEANUP="Nettoyage du monde (T_WORLD)"
M_LOGCLEANUP="Nettoyage des journaux du monde (T_WORLD)"
M_CHECKCONSISTENCY="Vérification de la cohérence du monde (T_WORLD)"
M_REPAIR="Réparation du monde (T_WORLD)"
M_WORLD_SYNCHRONIZATION="Synchronisation du monde"
M_GENERATING_SERVER="Génération des fichiers du serveur"
M_RUNNING_ALL_WORLDS="Mondes en cours d'exécution"
M_SOME_WORLDS_CRASHED="Certains mondes sont plantés"
M_SOME_WORLDS_INCONSTANCE="Certains mondes sont incohérents"

M_USAGE="Utilisation : $0
    download 'version'      - Télécharger le serveur
    create 'world' 'port'   - Crée un monde sur un port réseau particulier
    status 'world'          - Retourne l'état du monde
    start 'world'           - Démarrer le monde
    stop 'world'            - Arrêtet le monde
    restart 'world'         - Redémarret le monde
    sync 'world'            - Synchroniset le monde
    clean 'world'           - Nettoyer le monde
    backup 'world'          - Sauvegarder le monde
    repair 'world'          - Réparer le monde
    enable 'world'          - Activer le monde
    disable 'world'         - Désactiver le monde
    refresh 'world'         - Rafraîchir le monde
    op 'world' 'name'       - Ajouter un joueur en tant qu'opérateur
    deop 'world' 'name'     - Retirer le joueur des opérateurs
    kick 'world' 'name'     - Exclure un joueur
    ban 'world' 'name'      - Bannir un joueur
    unban 'world' 'name'    - Authoriser un joueur
    bs                      - Sauvegarde du serveur
    send 'command'          - Envoie une commande
    starts                  - Démarrer tous les mondes
    stops                   - Arrêter tous les mondes
    restarts                - Redémarrer tous les mondes
    syncs                   - Synchronise tous les mondes
    cleans                  - Nettoyer de tous les mondes
    backups                 - Sauvegarder tous les mondes
    servers                 - Lister tous les serveurs jar
    worlds                  - Lister tous les mondes
    running                 - Lister tous les mondes en cours d'exécution
    -force                  - Forcer la commande (doit être en premier)
"
