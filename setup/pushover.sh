#!/bin/bash
# LATEST: 2021-12-14

token=
user=
title=$1
message=$2

. [DIRECTORY]/files/pushover.conf

[PYTHON] [PUSHOVER] "$token" "$user" "$title" "$message" &

exit 0