import httplib, urllib, sys
conn = httplib.HTTPSConnection("api.pushover.net:443")
conn.request("POST", "/1/messages.json",
  urllib.urlencode({
    "token": sys.argv[1],
    "user": sys.argv[2],
    "title": sys.argv[3],
    "message": sys.argv[4],
  }), { "Content-type": "application/x-www-form-urlencoded" })
conn.getresponse()
